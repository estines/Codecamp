let peopleSalary = [
    { id: "1001", firstname: "Luke", lastname: "Skywalker", company: "Walt Disney", salary: "40000" },
    { id: "1002", firstname: "Tony", lastname: "Stark", company: "Marvel", salary: "1000000" },
    { id: "1003", firstname: "Somchai", lastname: "Jaidee", company: "Love2work", salary: "20000" },
    { id: "1004", firstname: "Monkey D", lastname: "Luffee", company: "One Piece", salary: "9000000" }
]

let peopleSalarynocompany = []

for (i in peopleSalary) {
    let dataget = {}
    let key = []
    key = Object.keys(peopleSalary[i])
    for (j in key){
        if (key[j] != 'company') {
            dataget[key[j]] = peopleSalary[i][key[j]]
        }
    }
    peopleSalarynocompany.push(dataget)
}

console.log(peopleSalarynocompany)