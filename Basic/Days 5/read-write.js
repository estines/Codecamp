let fs = require('fs')
let assert = require('assert')

let writeFiles = async (filename,content,callback) => {
    try {
        fs.writeFile(filename,content,'utf-8',err => {
            if (err) callback(err,null)
            else callback(null)
        })
    }
    catch (error) {
        console.log(error)
    }
}

let readFiles = async (filename,callback) => {
    try {
        fs.readFile(filename,'utf-8',(err,data) => {
            if (err) callback(err,null)
            else callback(null,data)
        })
    }
    catch (error) {
        console.log(error)
    }
}

// Unit Test
/*describe('Async Test', () => {
    describe('#ReadFiles()', () => {
        it('Should have content in text', done => {
            readFiles('demo.txt', (err,data) => {
                if (err) {
                    done(err)
                }
                else {
                    assert.deepEqual(data,'hello',"demo.txt should be \'hello\'")
                    done()
                }
            })
        })
    })
}) */

module.exports = {
    readFiles,
    writeFiles
}
//writeFiles('demo.txt','hello world')