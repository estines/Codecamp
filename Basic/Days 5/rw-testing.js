let fs = require('fs')
let assert = require('assert')
let {readFiles,writeFiles} = require('./read-write.js')

// Unit Test
describe('Async Test', () => {
    /* Only readFiles testing
    describe('#ReadFiles()', () => {
        it('Should have content in text', done => {
            readFiles('demo.txt', (err,data) => {
                if (err) {
                    done(err)
                }
                else {
                    assert.strictEqual(data.length >0 ,true,"demo.txt got content")
                    done()
                }
            })
        })
    })*/

    // Write file and check content in file
    describe('#WriteFiles()', () => {
        it('Should be create and write any content in text', (done) => {
            writeFiles('writedemo.txt','Write Complete!', (err) => {
                if (err) done(err)
                else {
                    readFiles('writedemo.txt', (err,data) => {
                        if (err) done(err)
                        else {
                            assert.deepEqual(data,'Write Complete!','Write function is working')
                        }
                        done()
                    })
                }
            })
        })
    })
})