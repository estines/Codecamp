let fields = ['id', 'firstname', 'lastname', 'company', 'salary']
let employees = [
    ['1001', 'Luke', 'Skywalker', 'Walt Disney', '40000'],
    ['1002', 'Tony', 'Stark', 'Marvel', '1000000'],
    ['1003', 'Somchai', 'Jaidee', 'Love2work', '20000'],
    ['1004', 'Monkey D', 'Luffee', 'One Piece', '9000000']
]

let peopleSalary = []

for (let i in employees) {
    let obj = {}
    for (let j=0; j<employees[i].length;j++) {
        obj[fields[j]] = employees[i][j]
    }
    peopleSalary.push(obj)
}
console.log(peopleSalary)