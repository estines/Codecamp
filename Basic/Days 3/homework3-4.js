let fs = require('fs')
let json = JSON.parse(fs.readFileSync('homework1-4.json'))
let newObject = []
let count = 0
let keys = []

json.forEach(function(item) {
    let objects = {}
    keys = Object.keys(item)
    keys.forEach(function (key) {
        if (key == 'name' || key == 'gender' || key == 'company' || key == 'email' || key == 'friends') {
            objects[key] = item[key]
        }  
    })
    newObject.push(objects)
    if (count < item.length){
        count++
    }
    else {
        count = 0
    }
})

console.log(newObject)
