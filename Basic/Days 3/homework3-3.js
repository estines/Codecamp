let students = 
    [
        { "id": "1001", 'firstname': 'Luke', 'lastname': 'Skywalker' },
        { "id": "1002", 'firstname': 'Tony', 'lastname': 'Stark' },
        { "id": "1003", 'firstname': 'Somchai', 'lastname': 'Jaidee' },
        { "id": "1004", 'firstname': 'Monkey D', 'lastname': 'Luffee' },
    ]

let company = [
    { "id": "1001", "company": "Walt Disney" },
    { "id": "1002", "company": "Marvel" },
    { "id": "1003", "company": "Love2work" },
    { "id": "1004", "company": "One Piece" },
]

let salary = [
    { "id": "1001", "salary": "40000" },
    { "id": "1002", "salary": "1000000" },
    { "id": "1003", "salary": "20000" },
    { "id": "1004", "salary": "9000000" },
]

let like = [
    { "id": "1001", "like": "apple" },
    { "id": "1002", "like": "banana" },
    { "id": "1003", "like": "orange" },
    { "id": "1004", "like": "papaya" },
];
let dislike = [
    { "id": "1001", "dislike": "banana" },
    { "id": "1002", "dislike": "orange" },
    { "id": "1003", "dislike": "papaya" },
    { "id": "1004", "dislike": "apple" },
]

let Databases = [students, company, salary, like, dislike]
let employeesDatabase = []
let fs = require('fs')

for (let i=0;i<Databases.length;i++){
    let objects = {}
    for( let j=0;j<Databases.length;j++){
        let properties = {}
        for (let k in Databases[j][i]){
            if (i == 0){
                properties[k] = Databases[j][i][k]
            }
            else if(k != 'id') {
                properties[k] = Databases[j][i][k]
            }
        }
        for(key in properties) {
            objects[key] = properties[key]
        }
        if(i != Databases[i].length){
            employeesDatabase[i] = objects
        }
    }
}

fs.writeFile('homework3-3.json',JSON.stringify(employeesDatabase),'utf8',function(err) {
    console.log('Write Success!!')
})