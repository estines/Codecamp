function readFile(text) {
    return new Promise(function (resolve, reject) {
        fs.readFile(text, 'utf8', function (err, content) {
            if (err) {
                reject(err)
            }
            else {
                resolve(content)
            }
        })
    })
}

function writeFile(filename, content) {
    return new Promise(function (resolve, reject) {
        fs.writeFile(filename, content, 'utf8', function (err) {
            if (err) {
                reject(err)
            }
            else {
                resolve()
            }
        })
    })
}

// Async Append
/*function appendContent(content) {
    return new promise(function (reject,resolve) {
        fs.appendFile('robot.txt', content, 'utf8', function(err) {
            if(err) {
                reject(err)
            }
            else {
                resolve()
            }
        })
    })
}*/

async function copyFiles(newFile, files) {
    try {
        let text = '', count = 0
        for (i in files) {
            let data = await readFile(files[i])
            if (count != files.length-1) {
                text += data + '\n'
            }
            else {
                text += data
            }
        }
        await writeFile(newFile,text)
    }
    catch (error) {
        console.error(error)
    }
}

let texts = { head: 'homework2-1/head.txt', body: 'homework2-1/body.txt', leg: 'homework2-1/leg.txt', feet: 'homework2-1/feet.txt' }
let robot = 'robot.txt'
let fs = require('fs')

copyFiles(robot, texts)