function readFile(text) {
    return new Promise(function (resolve, reject) {
        fs.readFile(text, 'utf8', function (err, content) {
            if (err) {
                reject(err)
            }
            else {
                resolve(content)
            }
        })
    })
}

function writeFile(filename, content) {
    return new Promise(function (resolve, reject) {
        fs.writeFile(filename, content, 'utf8', function (err) {
            if (err) {
                reject(err)
            }
            else {
                resolve()
            }
        })
    })
}

// Async Append
/*function appendContent(content) {
    return new promise(function (reject,resolve) {
        fs.appendFile('robot.txt', content, 'utf8', function(err) {
            if(err) {
                reject(err)
            }
            else {
                resolve()
            }
        })
    })
}*/

function copyFiles(newFile, files) {
    let promise = []
    for (let i in files) {
        promise.push(readFile(files[i]))
    }
    //await writeFile(newFile,text)
    Promise.all(promise)
    .then(function (allFile) {
        let count = 0, text = ''
        allFile.forEach(function (selectFile) {
            if (count != allFile.length - 1) {
                text += selectFile + '\n'
            }
            else {
                text += selectFile
            }
            count++
        })
        writeFile(newFile,text)
    })
}

let texts = { head: 'homework2-1/head.txt', body: 'homework2-1/body.txt', leg: 'homework2-1/leg.txt', feet: 'homework2-1/feet.txt' }
let robot = 'robot.txt'
let fs = require('fs')

copyFiles(robot, texts)