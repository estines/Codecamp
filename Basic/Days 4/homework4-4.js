let fs = require('fs')
let json = JSON.parse(fs.readFileSync('homework1-4.json','utf8'))
let newObj = [] , keys = ['name','gender','company','email','friends','balance']

newObj = json.filter(item => item.gender == 'male' && item.friends.length >= 2 )
.map(item => {
    let objects = {}
    keys.map(key => {
        if (key == 'balance') {
            let salary = item[key].toString().substr(1).split(',')
            let newsalary = ''
            salary = salary.map(num => {
                return newsalary = newsalary.concat(num)
            })
            newsalary = '$' + (parseFloat(newsalary)/10).toFixed(2)
            objects[key] = newsalary
        }
        else {
            objects[key] = item[key]
        }
    })
    return objects
})
console.log(newObj)