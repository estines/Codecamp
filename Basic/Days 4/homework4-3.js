let fs = require('fs')
let file = 'homework1.json', newObj = []

let readFiles = (fileName) => {
    return new Promise((resolve, reject) => {
        fs.readFile(fileName, 'utf8', (err, data) => {
            if (err) {
                reject(err)
            }
            else {
                resolve(data)
            }
        })
    })
}

let recordData = async (fileName) => {
    try {
        let json = JSON.parse(await readFiles(fileName))
        let totalSalaryDif = 0
        newObj = json.map(item => {
            let objects = {}
            objects['id'] = item.id
            objects['firstname'] = item.firstname
            objects['lastname'] = item.lastname
            objects['company'] = item.company
            if(item.salary < 100000) {
                item.salary = item.salary * 2
                objects['salary'] = item.salary
            }
            else {
                objects['salary'] = item.salary
            }
            totalSalaryDif += item.salary
            return objects
        })
        console.log(newObj)
        console.log('Total Salary to paid this month = ' + totalSalaryDif)
    }
    catch (error) {
        console.error(error)
    }
}

recordData(file)

