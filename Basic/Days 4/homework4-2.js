let fs = require('fs')
let file = 'homework1.json' , newObj = []

let readFiles = (fileName) => {
    return new Promise((resolve,reject) => {
        fs.readFile(fileName,'utf8',(err,data) => {
            if(err) {
                reject(err)
            }
            else {
                resolve(data)
            }
        })
    })
}

let recordData = async (fileName) => {
    try {
        let json = JSON.parse(await readFiles(fileName))
        newObj = json.map(item => {
            let objects = {}
            let keys = Object.keys(item)
            let fullname = ''
            keys.map(key => {
                if(key != 'salary') {
                    if (key == 'id') {
                        objects[key] = item.id
                    }
                    if (key == 'firstname') {
                        objects[key] = item.firstname
                        fullname += item.firstname
                    }
                    if (key == 'lastname') {
                        objects[key] = item.lastname
                        fullname += ' ' + item.lastname
                    }
                    if (key == 'company') {
                        objects[key] = item.company
                    }
                }
                else {
                    let newSalary = []
                    let baseSalary = item.salary
                    newSalary.push(Math.round(baseSalary*1))
                    newSalary.push(Math.round(baseSalary*1.1))
                    newSalary.push(Math.round((baseSalary*1.1)*1.1))
                    objects[key] = newSalary
                }
            })
            objects['fullname'] = fullname
            return objects
        })
        console.log(newObj)
    }
    catch (error) {
        console.error(error)
    }
}

recordData(file)

