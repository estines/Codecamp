import React, { Component } from 'react';
import { Card } from 'antd';
import './App.css';
import Display from './Components/display'
import ButtonPanel from './Components/button-panel'
import Calculate from './Controller/calculate'

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      total: null,
      next: null,
      operation: null
    }
  }

  onClickHandler = (btnName) => {
    this.setState(Calculate(this.state, btnName))
  }

  render() {
    return (
      <div className="App">
        <Card
          style={{ width: "60%" }}
        >
          <Display show={this.state.next || this.state.total || "0"} />
          <ButtonPanel clickHandler={this.onClickHandler} />
        </Card>
      </div>
    )
  }
}

export default App;
