import isNumber from './isnumber'
import Operation from './operation'
export default (obj, btnName) => {
    if (btnName === 'AC') {
        return {
            total: null,
            next: null,
            operation: null
        }
    }

    if (isNumber(btnName)) {
        if (btnName === '0' && obj.next === '0') return {}

        if (obj.operation) {
            if (obj.next) return { next: obj.next + btnName }
            return { next: btnName }
        }

        if (obj.next) return { next: obj.next + btnName, total: null }
        return { next: btnName }
    }

    if (btnName === '.') {
        if (!obj.next) return { next: 0 + btnName }
        if (obj.next.includes('.')) return {}
        return { next: obj.next + btnName }
    }

    if (btnName === '+/-') {
        if (obj.next) return { next: (-1 * parseFloat(obj.next)).toString() }
        if (obj.total) return { next: (-1 * parseFloat(obj.next)).toString() }
        return {}
    }

    if (btnName === '=') {
        if (obj.next && obj.operation) {
            return {
                total: Operation(obj.total, obj.next, obj.operation),
                next: null,
                operation: null
            }
        }
        return {}
    }

    if (obj.operation) {
        return {
            total: Operation(obj.total, obj.next, obj.operation),
            next: null,
            operation: null
        }
    }

    if (!obj.next) {
        return { operation: btnName }
    }

    return {
        total: obj.next,
        next: null,
        operation: btnName
    }

}