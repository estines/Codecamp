export default (total, next, operation) => {
    const first = parseFloat(total)
    const second = parseFloat(next)
    if (operation === '+') {
        return (first + second).toString()
    }

    if (operation === '-') {
        return (first - second).toString()
    }

    if (operation === '*') {
        return (first * second).toString()
    }

    if (operation === '/') {
        return (first / second).toString()
    }

    if (operation === '%') {
        return (first % second).toString()
    }

}