import React, { Component } from 'react'
import Button from './button'
import './button-panel.css'
class ButtonPanel extends Component {

    onClickHandler = btnName => {
        this.props.clickHandler(btnName)
    }

    render() {
        return (
            <div className="btn-row">
                <div>
                    <Button btnHandler={this.onClickHandler} name="AC" menu/>
                    <Button btnHandler={this.onClickHandler} name="+/-" menu />
                    <Button btnHandler={this.onClickHandler} name="%" menu />
                    <Button btnHandler={this.onClickHandler} name="/" operator />
                </div>
                <div>
                    <Button btnHandler={this.onClickHandler} name="7" />
                    <Button btnHandler={this.onClickHandler} name="8" />
                    <Button btnHandler={this.onClickHandler} name="9" />
                    <Button btnHandler={this.onClickHandler} name="*" operator />
                </div>
                <div>
                    <Button btnHandler={this.onClickHandler} name="4" />
                    <Button btnHandler={this.onClickHandler} name="5" />
                    <Button btnHandler={this.onClickHandler} name="6" />
                    <Button btnHandler={this.onClickHandler} name="-" operator />
                </div>
                <div>
                    <Button btnHandler={this.onClickHandler} name="1" />
                    <Button btnHandler={this.onClickHandler} name="2" />
                    <Button btnHandler={this.onClickHandler} name="3" />
                    <Button btnHandler={this.onClickHandler} name="+" operator />
                </div>
                <div>
                    <Button btnHandler={this.onClickHandler} name="0" wide />
                    <Button btnHandler={this.onClickHandler} name="." />
                    <Button btnHandler={this.onClickHandler} name="=" operator />
                </div>
            </div>
        )
    }
}

export default ButtonPanel;