import React, { Component } from 'react';
import './button.css'
import { Button } from 'antd'

class Buttons extends Component {

    handlerClick = () => {
        this.props.btnHandler(this.props.name) 
    }

    render() {

        const btnStyle = [
            "btn",
            this.props.operator ? "opt" : " ",
            this.props.menu ? "menu" : " ",
            this.props.wide ? "wide" : " "
        ]

        return (
            <Button className={btnStyle} onClick={this.handlerClick}>
                {this.props.name}
            </Button>
        )
    }
}

export default Buttons;