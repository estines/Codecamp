import React, { Component } from 'react';
import InputBox from './input-box'
import Header from './header'
import { Card } from 'antd'

class Todo extends Component {
    render() {
        return (
            <div style={{ margin: '5% auto', width: '500px' }}>
                <Card title={<Header />} style={{ width: 500 }}>
                    <InputBox />
                </Card>
            </div>
        );
    }
}

export default Todo