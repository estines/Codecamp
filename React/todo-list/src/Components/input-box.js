import React from 'react'
import { Input, Spin } from 'antd'
import Task from './task'

const Search = Input.Search
const url = "http://localhost:4000/todo/"

class InputBox extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            inputText: '',
            listItem: [],
            isLoading: true,
            isCompleted: null
        }
        this.handleChangeText = this.handleChangeText.bind(this)
    }

    componentDidMount() {
        this.fetchGet(url)
    }

    async fetch(url, method) {
        this.setState({ isLoading: true })
        const res = await fetch(url, method)
        this.setState({ isLoading: false })
        return res.json()
    }

    async fetchGet() {
        let listItem = await this.fetch(url)
        listItem = listItem.map(value => {
            return { ...value }
        })
        this.setState({ listItem })
    }

    async fetchPost(text) {
        const result = await this.fetch(url, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                text
            })
        })

        if (result) {
            this.setState({
                listItem: [...this.state.listItem, { id:result, text}]
            })
        }

    }

    async fetchDelete(index, id) {
        let res = await this.fetch(url + id, {
            method: 'DELETE',
        })
        console.log(res)
        if (res) {
            const listItem = this.state.listItem.filter(item =>
                item.id !== id
            )
            this.setState({ listItem })
        }
    }

    submitList = () => {
        this.fetchPost(this.state.inputText)
        this.setState({ inputText: '' })
    }

    handleChangeText = (event) => {
        this.setState({ inputText: event.target.value });
    }

    deleteTaskItem = (index, id) => {
        this.fetchDelete(index, id)
    }

    render() {
        return (
            <div>
                <Search
                    placeholder="input todo list"
                    onSearch={this.submitList}
                    onChange={this.handleChangeText}
                    value={this.state.inputText}
                    enterButton="Add"
                    size="large"
                    style={{ margin: "0 auto" }}
                />
                {this.state.isLoading === false ?
                    <Task taskItem={this.state.listItem} deleteItem={this.deleteTaskItem} />
                    : <Spin />
                }
            </div>
        )
    }
}

export default InputBox