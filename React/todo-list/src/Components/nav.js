import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import './nav.css'
class Nav extends Component {
    render() {
        return (
            <div class="topnav">
                <a><Link to="/">Home</Link></a>
                <a><Link to="/todo1">Todo1</Link></a>
                <a><Link to="/todo2">Todo2</Link></a>
                <a><Link to="/todo3">Todo3</Link></a>
            </div>
        )
    }
}

export default Nav