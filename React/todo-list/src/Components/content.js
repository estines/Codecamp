import React, { Component } from 'react'
import { Switch, Route } from 'react-router-dom'
import Home from './home'
import TodoWithKoa from './todo'
import TodoWithFirebase from './todo_firebase'
import TodoWithMockApi from './todo_mockapi'


class Content extends Component {
    render() {
        return (
            <main>
                <Switch>
                    <Route exact path="/" component={Home} />
                    <Route path="/todo1" component={TodoWithKoa} />
                    <Route path="/todo2" component={TodoWithFirebase} />
                    <Route path="/todo3" component={TodoWithMockApi} />
                </Switch>
            </main>   
        )
    }
}

export default Content;