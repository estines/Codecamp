import React from 'react'
import { List, Button } from 'antd';
// import InfiniteScroll from 'react-infinite-scroller'

class Task extends React.Component {
    render() {
        let { taskItem,deleteItem } = this.props
        return (
            <div style={{height:'300px',overflow: 'scroll'}}>
                <List
                    style={{ marginTop: "15px" }}
                    bordered
                    dataSource={taskItem}
                    size="small"
                    renderItem={(item,index) => (
                        <List.Item actions={[<Button 
                            type="danger"
                            shape="circle"
                            icon="close"
                            size="small"
                            onClick={() => deleteItem(index,item.id)}
                        />]}>
                            {item.text}
                        </List.Item>
                    )}
                />
            </div>
        )
    }
}

export default Task