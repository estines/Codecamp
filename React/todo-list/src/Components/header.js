import React from 'react'

class Header extends React.Component {
    render() {
        return (
            <div>
                <h2 style={{margin:"0", textAlign:"center"}}>To-Do-List</h2>
            </div>
        )
    }
}

export default Header