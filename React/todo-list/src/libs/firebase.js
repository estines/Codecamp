import * as firebase from 'firebase'

const config = {
  apiKey: "AIzaSyAWvw3hAa2YhtDfgHEg8F9v_K7bS_aMYvw",
  authDomain: "test-1516704077097.firebaseapp.com",
  databaseURL: "https://test-1516704077097.firebaseio.com",
  projectId: "test-1516704077097",
  storageBucket: "test-1516704077097.appspot.com",
  messagingSenderId: "113321152258"
}

class Firebase {

  constructor() {
    this.firebase = firebase.initializeApp(config)
  }

  getDatabase() {
    return this.firebase.database();
  }

}

export default new Firebase().getDatabase();