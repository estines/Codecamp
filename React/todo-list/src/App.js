import React, { Component } from 'react';
import Nav from './Components/nav'
import Content from './Components/content'

class App extends Component {
  render() {
    return (
      <div>
        <Nav />
        <Content />
      </div>
    )
  }
}

export default App;