import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Layout from './Components/layout'
import Left from './Components/content-left'
import Right from './Components/content-right'
import Footer from './Components/footer'
import Signin from './Components/signin'
import Welcome from './Components/welcome'
import Signup from './Components/signup'
import registerServiceWorker from './registerServiceWorker'

ReactDOM.render(<Layout />, document.getElementById('root'))
ReactDOM.render(<Left />, document.getElementById('ctn-left'))
ReactDOM.render(<Right />, document.getElementById('ctn-right'))
ReactDOM.render(<Signin />, document.getElementById('signin'))
ReactDOM.render(<Welcome />, document.getElementById('welcome'))
ReactDOM.render(<Signup />, document.getElementById('signup'))
ReactDOM.render(<Footer />, document.getElementById('ctn-footer'))
registerServiceWorker()