import React from 'react'
import { Icon } from 'antd'
class Left extends React.Component {
    render() {
        let h2Style = {
            marginBottom: '30px',
        }
        let h2FirstChild = {
            marginTop: '-45px',
            marginBottom: '30px'
        }
        return (
            <div className="content-left">
                <Icon id="twitter-ico" type="twitter" ></Icon>
                <h2 style={h2FirstChild}><Icon type="search" />Follow your interests.</h2>
                <h2 style={h2Style}><Icon type="usergroup-add" />Hear what people are talking about.</h2>
                <h2><Icon type="message" />Join the conversation.</h2>
            </div>
        )
    }
}
export default Left