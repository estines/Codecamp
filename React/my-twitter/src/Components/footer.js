import React from 'react'

class Footer extends React.Component {
    render() {
        let clear = {
            margin: '0'
        }
        return (
            <div className="footer">
                <span className="nav-footer">
                    <a>About</a>
                    <a>Help Center</a>
                    <a>Blog</a>
                    <a>Status</a>
                    <a>Jobs</a>
                    <a>Terms</a>
                    <a>Privacy Policy</a>
                    <a>Cookies</a>
                    <a>Ads info</a>
                    <a>Brand</a> 
                    <a>Apps</a>
                    <a>Advertise</a>
                    <a>Marketing</a>
                    <a>Businesses</a>
                    <a>Developers</a>
                    <a>Directory</a>
                    <a>Settings</a>
                    <a style={clear}>© 2018 Twitter</a>
                </span>
            </div>
        )
    }
}

export default Footer