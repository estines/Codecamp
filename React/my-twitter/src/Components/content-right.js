import React from 'react'

class Right extends React.Component {
    render() {
        return (
            <div className="content-right">
                <div id="signin"></div>
                <div id="welcome"></div>
                <div id="signup"></div>
            </div>
        )
    }
}

export default Right