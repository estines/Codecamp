import React from 'react'
import { Icon } from 'antd'

class Welcome extends React.Component {
    render () {
        let align = {
            marginLeft: '10%',
            fontSize: '16px'
        }
        let icon = {
            marginLeft: '10%',
            fontSize: '325%',
            color: '#55acee'
        }
        let width = {
            width: '60%',
            margin: '15px auto'
        }
        return (
            <div style={width}>
                <Icon style={icon} type="twitter" />
                <div style={align}>
                <h1>See what's happening in the world right now</h1>
                <h3>Join Twitter today.</h3>
                </div>
            </div>
        )
    }
}

export default Welcome