import React from 'react'
import { Row, Col } from 'antd'

class Layout extends React.Component {
    render() {
        return (
            <div>
                <Row>
                    <Col id="ctn-left" span={12}>Left</Col>
                    <Col id="ctn-right" span={12}>Right</Col>
                </Row>
                <Row>
                    <Col id="ctn-footer" span={24}>Footer</Col>
                </Row>
            </div>
        )
    }
}

export default Layout