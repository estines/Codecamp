import React, { Component } from 'react'
import { Input, Button } from 'antd'
import './chatbox.css'
class ChatBox extends Component {

    render() {
        let { onClicks, onEnter, getValue } = this.props

        return (
            <div className="chatbox">
                <Input
                    placeholder="Enter your message"
                    onPressEnter= {(e)=> onEnter(e)}
                    onChange= {(e)=> getValue(e)}
                />
                <Button
                    onClick={() => onClicks()}
                >
                    Send
                </Button>
            </div>
        )
    }
}

export default ChatBox;