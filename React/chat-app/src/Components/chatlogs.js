import React, { Component } from 'react'
import Message from './message'
import { List } from 'antd'
class ChatLogs extends Component {
    render() {
        let { recentMsg } = this.props
        return (
            <List
                dataSource={recentMsg}
                renderItem={(text, index) => (
                    <Message msg={text} value={index}/>
                )}
            />
        )
    }
}

export default ChatLogs;