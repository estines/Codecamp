import React, { Component } from 'react'
import './message.css'
import { Avatar } from 'antd';

class Message extends Component {
    render() {
        let msgType = [
            "messages",
            this.props.value % 2 === 0 ? "" : "receiver",
        ]

        let msgSide = [
            "show-msg",
            this.props.value % 2 === 0 ? "" : "receiver"
        ]

        return (
            <div className={msgType.join('').trim()}>
                <div className={msgSide}>
                    <Avatar size="large" icon="user" />
                    <span className="message-text">
                        {this.props.msg}
                    </span>
                </div>
            </div >
        )
    }
}

export default Message;