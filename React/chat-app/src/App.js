import React, { Component } from 'react';
import ChatLogs from './Components/chatlogs'
import ChatBox from './Components/chatbox'
import { Icon, Card } from 'antd'
import './App.css'
class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      inputText: "",
      textList: [],
    }
  }

  submitList = () => {
    this.setState({
      textList: this.state.textList.concat([this.state.inputText]),
      inputText: ''
    })

  }

  handleKeyPress = (event) => {
    this.submitList()

  }

  handleChangeText = (event) => {
    this.setState({ inputText: event.target.value });
  }

  render() {
    return (
      <div className="App">
        <Card title="Message"
          extra={<Icon type="close-circle-o" />}
        >
          <div className="messagebox">
            <ChatLogs recentMsg={this.state.textList}/>
          </div>
          <ChatBox 
            onClicks={this.submitList}
            onEnter={this.handleKeyPress}
            getValue={this.handleChangeText}
          />
        </Card>
      </div>
    )
  }
}

export default App;