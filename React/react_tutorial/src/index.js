import React from 'react';
import ReactDOM from 'react-dom';
import './index.css'
// import App from './App'
import Header from './Components/header'
import Carousel from './Components/carousel'
import registerServiceWorker from './registerServiceWorker'
ReactDOM.render(<Header />,document.getElementById("header"))
ReactDOM.render(<Carousel />, document.getElementById("body"))
registerServiceWorker()