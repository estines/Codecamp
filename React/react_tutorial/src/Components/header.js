import React from 'react'
import { Menu } from 'antd'

class Navbar extends React.Component {
    render() {
        return (
            <div className="navbar">
                <Menu
                    mode="horizontal"
                    theme="dark"
                >
                    <Menu.Item key="product">
                        <h3>Product</h3>
                    </Menu.Item>
                    <Menu.Item key="About">
                        <h3>About</h3>
                    </Menu.Item>
                </Menu>
            </div>
        )
    }
}

export default Navbar