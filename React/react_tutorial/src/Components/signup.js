import React from 'react'
import { Card, Form, Icon, Input, Button, Checkbox } from 'antd'
const FormItem = Form.Item

class NormalLoginForm extends React.Component {
    handleSubmit = (e) => {
        e.preventDefault()
        this.props.form.validateFields((err, values) => {
            if (!err) {
                console.log('Received values of form: ', values)
            }
        })
    }
    render() {
        const { getFieldDecorator } = this.props.form
        return (
            <div className="Content">
                <h1 id="header">Welcome to my Application.</h1>
                <Card title="Sign In" className="Form">
                    <Form onSubmit={this.handleSubmit} className="login-form">
                        <label>User Name</label>
                        <FormItem>
                            {getFieldDecorator('userName', {
                                rules: [{ required: true, message: 'Please input your username!' }],
                            })(
                                <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Username" />
                                )}
                        </FormItem>
                        <label>Password</label>
                        <FormItem>
                            {getFieldDecorator('password', {
                                rules: [{ required: true, message: 'Please input your Password!' }],
                            })(
                                <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Password" />
                                )}
                        </FormItem>
                        <FormItem>
                            {getFieldDecorator('remember', {
                                valuePropName: 'checked',
                                initialValue: true,
                            })(
                                <Checkbox>Remember me</Checkbox>
                                )}
                            <Button type="primary" htmlType="submit" className="form-button">
                                Sign In
                        </Button>
                            Or <a href="">register now!</a>
                        </FormItem>
                        <label>Forget Password</label>
                        <FormItem>
                            {getFieldDecorator('e-mail', {
                                rules: [{ required: true, message: 'Please input your E-mail!' }],
                            })(
                                <Input prefix={<Icon type="mail" style={{ color: 'rgba(0,0,0,.25)' }} />} type="email" placeholder="E-mail" />
                                )}
                            <Button type="primary" htmlType="submit" className="form-button">
                                Sign Up
                            </Button>
                        </FormItem>
                    </Form>
                </Card>
            </div>
        )
    }
}

const WrappedNormalLoginForm = Form.create()(NormalLoginForm)

export default WrappedNormalLoginForm