const koa = require('koa')
const bodyparser = require('koa-bodyparser')
const multer = require('koa-multer')
const upload = multer({ dest: 'upload/' })
const jimp = require('jimp')
const fs = require('fs')
const mysql = require('mysql2/promise')
const Router = require('koa-router')
const render = require('koa-ejs')
const path = require('path')
const _ = require('lodash')
const session = require('koa-session')

module.exports = { 
    koa,
    bodyparser,
    multer,
    upload,
    jimp,
    fs,
    mysql,
    Router,
    render,
    path,
    session,
    _
}