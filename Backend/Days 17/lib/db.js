const mysql = require('mysql2/promise')

module.exports = () => {
    const pool = mysql.createPool({
        host: 'localhost',
        user: 'root',
        password: 'root',
        database: 'todo'        
    })

    return pool
}