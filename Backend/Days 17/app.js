const { koa, session } = require('./config/modules')
const Session = require('./model/session')
const pool = require('./lib/db')()

const sessionConfig = {
    key: 'sess',
    maxAge: 3600,
    httpOnly: true,
    store: {
        async get(key, maxAge, { rolling }) {
            return Session.get(pool, key)
        },
        async set(key, sess, maxAge, { rolling }) {
            Session.set(pool, key, sess)
        },
        async destroy(key) {
            Session.destroy(pool, key)
        }
    }
}
const app = new koa()

app.keys = ['kepp session']

app.use(session(sessionConfig, app))
app.use(handler)
app.listen(3000)

async function handler (ctx) {
    count = ctx.session.views || 0
    ctx.session.views = ++count
    ctx.body = `${count} views`
}