const koa = require('koa'), Router = require('koa-router'), bodyparser = require('koa-bodyparser'), bcrypt = require('bcryptjs')
const app = new koa()

const router = new Router()
    .post('/', encode)

app.use(bodyparser())
app.use(router.routes())

async function encode (ctx) {
    const { password } = ctx.request.body
    console.log(password)
    if (!password) {
        ctx.throw(400)
    }
    const hash = await bcrypt.hash(password,10)
    ctx.body = { hash }
}

app.listen(3000)