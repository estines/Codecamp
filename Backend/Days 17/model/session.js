const { mysql } = require('../config/modules')

module.exports = {
    get: async ( db, key) => {
        try {
            let [sess] = await db.query(` select s_key, s_values from sessions where s_key = ?`, [key])
            console.log(sess[0])
            return JSON.parse(sess[0].s_values)
        } catch (err) {
            console.error(`Error : ${err}`)
        }
    },
    set: async (db, key, sess, count) => {
        try {
            await db.query(`insert into sessions(s_key,s_values) 
            values( ?, ? )`, [key, JSON.stringify(sess)])
        }
        catch (err) {
            await db.query(`update sessions set s_values = ? where s_key = ?`
                , [JSON.stringify(sess), key])
            console.error(`Error : ${err}`)
        }
    },
    destroy: async (db, key) => {
        try {
            await db.query(`delete from sessions where s_key = ?`, [key])
        }
        catch (err) {
            console.error(`Error : ${err} `)
        }
    }
}