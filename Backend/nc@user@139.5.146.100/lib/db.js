const { config } = require('../config/database')
const mysql = require('mysql2/promise')

module.exports = app => {
    app.use(async (ctx, next) => {
        ctx.db = mysql.createPool({
            host: config.host,
            user: config.user,
            password: config.password,
            database: config.database
        })
        await next()
    })
}
