const _ = require('lodash')

module.exports = {
    list: async ctx => {
        let [result] = await ctx.db.query(`select users.id, users.text, status.status_name as status 
            from users join status on status.id = users.status_id`)
        let text = ''
        result.map(row => {
            text += `id: ${row.id}, text: ${row.text}, status: ${row.status}\n`
        })
        ctx.body = result
    },
    create: async ctx => {
        let { text } = ctx.request.body
        if (!_.isString(text)) {
            ctx.status = 400
            ctx.body = {
                error: 'text must be string'
            }
            return
        }

        text = text.trim()
        if (text.length === 0 || text.length > 200) {
            ctx.status = 400
            ctx.body = {
                error: 'text length must be 1 to 200'
            }
            return
        }

        let [result] = await ctx.db.execute(`
            insert into users(text) 
            values (?) `, [text])

        ctx.body = result.insertId
    },
    get: async ctx => {
        const id = ctx.params.id
        await ctx.db.execute(`
            select users.id, users.text, status.status_name as status from users
            join status on status.id = users.status_id
            where users.id = ?` , [id])
            .then(([row]) => {
                ctx.body = `Get User > id: ${row[0].id} text: ${row[0].text} status: ${row[0].status}`
            })
    },
    update: async ctx => {
        const id = ctx.params.id
        let { text } = ctx.request.body
        await ctx.db.execute(`
            update users set text = ?
            where id = ?`, [text, id])
            .then(ctx.body = 'Update Complete!!')
    },
    remove: async ctx => {
        const id = ctx.params.id
        await ctx.db.execute(`
            delete from users where id = ? `, [id])
            .then(ctx.body = { delete: 'ok' })
    },
    complete: async ctx => {
        const id = ctx.params.id
        await ctx.db.execute(`
            update users set status_id = '200' where id = ?`, [id])
            .then(ctx.body = { complete: `User id: ${id} > Status Change to Complete!!` })
    },
    incomplete: async ctx => {
        const id = ctx.params.id
        await ctx.db.execute(`
            update users set status_id = '100' where id = ?`, [id])
            .then(ctx.body = { incomplete: `User id: ${id} > Status Change to Incomplete!!` })
    }
}