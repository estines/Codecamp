const { modules_path } = require('./config/modules')
const { config } = require('./config/database')
const { koa, Router, fs, bodyparser, mysql } = require(modules_path)
const Todo = require('./controller/todo')
const app = new koa()

const router = new Router()
    .get('/todo', Todo.list)
    .post('/todo', Todo.create)
    .get('/todo/:id', Todo.get)
    .patch('/todo/:id', Todo.update)
    .delete('/todo/:id', Todo.remove)
    .put('/todo/:id/complete', Todo.complete)
    .delete('/todo/:id/complete', Todo.incomplete)

require('./lib/db')(app)

app.use(bodyparser())
app.use(router.routes())
app.listen(3000)