const koa = require('koa')
const Router = require('koa-router')
const fs = require('fs')
const path = require('path')

const router = new Router()
    .get('/', index)
    .get('/about', about)
    .post('/login', login)

new koa ()
    .use (router.routes())
    .listen(3000)

async function index (ctx) {
    ctx.body = 'index page'
}

function about (ctx) {
    ctx.body = 'about page'
}

function login (ctx) {
    ctx.body = 'login page'
}