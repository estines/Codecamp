const koa = require('koa')
const app = new koa()

app.use(requestLogger)
app.use(handler)
app.listen(3000)


async function handler(ctx, next) {
    ctx.body = 'test'
    throw 'err:555'
    await next()
}

async function requestLogger(ctx, next) {
    await next()
    console.log(`${ctx.method} ${ctx.path}`)
}

async function errorRecovery(ctx, next) {
    try {
        await next()
    }
    catch(err) {
        console.log('Server Error: ' + err)

    }
}