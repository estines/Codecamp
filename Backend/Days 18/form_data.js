const koa = require('koa')
const multer = require('koa-multer')

const upload = multer({dest: 'upload/'})

const app = new koa()

app.use(async (ctx) => {
    await upload.single('file')(ctx)
    ctx.body = ctx.req.file.path
    console.log(ctx.req.file.path)
}).listen(3000)