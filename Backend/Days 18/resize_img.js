const { koa, multer, upload, jimp , fs} = require('../const')
const app = new koa()

app.use(async (ctx) => {
    await upload.single('file')(ctx)
    const tempFile = ctx.req.file.path
    const outFile = tempFile + '.jpg'
    jimp.read(tempFile),(err,img) => {
        if (err) throw err
        img.resize(100,100).write(outFile)
    }

    fs.unlink(tempFile, () => {} )
    ctx.body = outFile
}).listen(3000)