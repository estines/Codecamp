module.exports = {
  create,
  getUserInfo,
  getUsername,
  changeStatus,
  changePhoto,
  changeCover,
  sendChat,
  receiveChat,
  getChat,
  follow,
  unfollow
}

async function create(db, user) {
  const result = await db.execute(`
    insert into users (
      username, email, password, name, status
    ) values (
      ?, ?, ?, ?, ?
    )
  `, [
      user.username, user.email, user.password,
      user.name, 0
    ])
  return result[0].insertId
}

async function getUserInfo(db, username, email) {
  try {
    if (username) {
      const [result] = await db.execute(`select id, password from users where username = ?`, [username])
      return result[0]
    }
    const [result] = await db.execute(`select id, password from users where email = ?`, [email])
    return result[0]
  }
  catch (err) {
    return undefined
  }
}

async function getUsername(db, userId) {
  const [result] = await db.execute(`select username from users where id = ?`, [userId])
  return result[0].username
  await next()
}

async function changeStatus(db, id, status) {
  try {
    await db.execute(`update users set status = ? where id = ?`, [status, id])
  }
  catch (err) {
    console.error(`Error: ${err}`)
    return
  }
}

async function changePhoto(db, userId, photoUrl) {
  await db.execute(`
    update users set
      photo = ?
    where id = ?
  `, [photoUrl, userId])
}

async function changeCover(db, userId, photoUrl) {
  await db.execute(`
    update users set
      cover = ?
    where id = ?
  `, [photoUrl, userId])
}

async function sendChat(db, sender, receiver, content, type) {
  const result = await db.execute(`insert into user_chats (
    sender_id, receiver_id, content, type ) 
    values ( ? , ? , ?, ? )`, [sender, receiver, content, type])
  return result[0].insertId
}

async function receiveChat(db, receiver) {
  const [result] = await db.execute(`select sender_id, content, type, created_at from user_chats where receiver_id = ?`, [receiver])
  return result
}

async function getChat(db, userId) {
  let [result] = await db.execute(`select DISTINCT receiver_id from user_chats where sender_id = ?`, [userId])
  return result
}

async function follow(db, followerId, followingId) {
  await db.execute(`
    insert into follows (
      follower_id, following_id
    ) values (
      ?, ?
    )
  `, [followerId, followingId])
}

async function unfollow(db, followerId, followingId) {
  await db.execute(`
    delete from follows
    where follower_id = ? and following_id = ?
  `, [followerId, followingId])
}