module.exports = {
  create,
  addPhoto,
  addHashtag,
  like,
  unlike,
  retweet
}

async function create(db, userId, tweet) {
  const result = await db.execute(`
    insert into tweets (
      user_id, content, type
    ) values (
      ?, ?, ?
    )
  `, [
      userId, tweet.content, tweet.type
    ])
  return result[0].insertId
}

async function addPhoto(db, tweetId, photoUrl) {
  const result = await db.execute(`
    insert into tweet_photos (
      tweet_id, url
    ) values (
      ?, ?
    )
  `, [
      tweetId, photoUrl
    ])
  return result[0].insertId
}

async function addHashtag(db, tweetId, hashtag) {
  await db.execute(`
    insert into tweet_hashtags (
      tweet_id, hashtag 
      values (
        ?, ?
      )
    `, [
      tweetId, hashtag
    ])
}

async function like(db, userId, tweetId) {
  await db.execute(`
    insert into tweet_likes (
      user_id, tweet_id
    ) values (
      ?, ?
    )
  `, [
      userId, tweetId
    ])
}

async function unlike(db, userId, tweetId) {
  await db.execute(`
    delete from tweet_likes
    where user_id = ? and tweet_id = ?
  `, [userId, tweetId])
}

async function retweet(db, userId, tweetId, content) {
  await db.execute(`
  insert into retweets  (user_id, tweet_id, content)
  values (?, ?, ?) `, [userId, tweetId, content])
}