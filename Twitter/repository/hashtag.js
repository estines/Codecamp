module.exports = {
    create,
    list
}

async function create(db, hashtag) {
    await db.execute(`insert into hashtags(name) values (?) `, [hashtag])
}

async function list(db, hashtag) {
    let [result] = await db.execute(`select tweets.id, tweets.user_id, tweets.content
    from tweet_hashtags
    join tweets on tweets.id = tweet_hashtags.tweet_id
    join hashtags on hashtags.name = tweet_hashtags.hashtag
    where tweet_hashtags.hashtag = ?`, [hashtag])
}