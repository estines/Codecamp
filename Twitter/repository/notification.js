module.exports = {
  create,
  list,
  markAsRead
}

async function create(db, userId, noti) {
  const result = await db.execute(`
    insert into notifications (
      user_id, title, content, photo
    ) values (
      ?, ?, ?, ?
    )
  `, [
      userId, noti.title, noti.content, noti.photo
    ])
  return result[0].insertId
}

async function list(db, userId) {
  const [result] = await db.execute(`select title, content, photo, is_read, created_at 
    from notifications where user_id = ? and is_read != true` , [userId])
  return result
}

async function markAsRead(db, notiId) {
  await db.execute(`
    update notifications set
      is_read = true
    where id = ?
  `, [notiId])
}