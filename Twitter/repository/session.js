module.exports = {
    get,
    set,
    destroy,
    addUser
}

async function get(db, key) {
    try {
        let [sess] = await db.execute(`
        select id, history, user_id from sessions
        where id = ? `, [key])
        return sess[0]
    }
    catch (err) {
        console.log(`Viewer's session generated!!`)
    }
}

async function set(db, key, sess) {
    try {
        await db.execute(`insert into sessions( id, history) 
        values(?, ?)`, [key, JSON.stringify(sess)])
    }
    catch (err) {
        await db.execute(`update sessions set history = ?
            where id = ?`, [JSON.stringify(sess), key])
    }
}

async function destroy(db, key) {
    try {
        await db.execute(`delete from sessions where id = ?`, [key])
    }
    catch (err) {
        console.error(`Error : ${err} `)
    }
}

async function addUser(db, key, userId) {
    try{
        await db.execute(`update sessions set user_id = ? where id = ?`, [userId, key])
    }
    catch(err) {
        console.error(`Error: ${err}`)
    }
}