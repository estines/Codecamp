const config = require('../config/database.json')
const { mysql } = require('../config/modules')

module.exports = () => {
    return mysql.createPool({
        host: config.host,
        user: config.user,
        password: config.password,
        database: config.database
    })  
}