const { Router, bodyparser } = require('../config/modules')
const Autherize = require('./checkingSession'),
    auth = require('./Authentication'),
    msg = require('./Directmsg'),
    noti = require('./notifications'),
    user = require('./Users')

const router = new Router()
    .use(Autherize.checkSess)
    //! --- Authentication ---
    .post(`/auth/signup`, auth.signup)
    .post(`/auth/signin`, auth.signin)
    .get(`/auth/signout`, auth.signout)
    //.post(`/auth/verify`, auth.verify)
    //! --- Direct Message ---
    .get(`/message`, msg.get)
    .get(`/message/:userid`, msg.receive)
    .post(`/message/:userid`, msg.send)
    //! --- Notification ---
    .get(`/notification`, noti.list)
    //! --- Tweets ---
    // .get(`/tweet`, tweet.list)  
    // .post(`/tweet`, tweet.create)
    // .put(`/tweet/:id/like`, tweet.like)
    // .delete(`/tweet/:id/like`, tweet.unlike)
    // .post(`/tweet/:id/retweet`, tweet.retweet)
    // .put(`/tweet/:id/voteid`, tweet.vote)
    // .post(`/tweet/:id/reply`, tweet.reply)
    //! --- Uploads ---
    // .post(`/upload`, upload.upload)
    //! --- Users ---
    //.patch(`/user/:id`, user.update)
    .put(`/user/:id/follow`, user.follow)
    .delete(`/user/:id/follow`, user.unfollow)
// .get(`/user/:id/follow`, user.following)
// .get(`/user/:id/followed`, user.follower)

module.exports = app => {
    app.use(bodyparser())
    app.use(router.routes())
}


// --- Old Routes --- //
//// const Auth = require('./Routes/auth')
//// const DirectMsg = require('./Routes/directmsg')
//// const Notifications = require('./Routes/noti')
//// const Tweets = require('./Routes/tweets')
//// const Uploads = require('./Routes/uploads')
//// const Users = require('./Routes/users')
//// const Auth = require('../controller/authenticate')

// Auth(app, router)   //! Sending router to other middleware //
// DirectMsg(app, router)
// Notifications(app, router)
// //// Tweets(app,router)
// //// Uploads(app,router)
// Users(app, router)
// }