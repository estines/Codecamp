module.exports = (app, router) => {
    router.patch(`/user/:id`, user.update)
        .put(`/user/:id/follow`, user.follow)
        .delete(`/user/:id/follow`, user.unfollow)
        .get(`/user/:id/follow`, user.following)
        .get(`/user/:id/followed`, user.follower)

    app.use(router.routes())
}