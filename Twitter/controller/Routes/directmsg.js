const Auth = require('../authenticate')
const msg = require('../Control/Directmsg')
module.exports = (app, router) => {
    router
        .use(Auth.checkSess)
        .get(`/message`, msg.get)
        .get(`/message/:userid`, msg.receive)
        .post(`/message/:userid`, msg.send)

    app.use(router.routes())
}