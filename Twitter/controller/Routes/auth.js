const auth = require('../Control/Authentication')
const Auth = require('../authenticate')
module.exports = (app, router) => {
    router
        .use(Auth.checkSess)
        .post(`/auth/signup`, auth.signup)
        .post(`/auth/signin`, auth.signin)
        .get(`/auth/signout`, auth.signout)
        .post(`/auth/verify`, auth.verify)
    app.use(router.routes())
}