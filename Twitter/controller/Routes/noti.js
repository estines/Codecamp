const Auth = require('../authenticate')
const Noti = require('../Control/notifications')

module.exports = (app, router) => {
    router.use(Auth.checkSess)
        .get(`/notification`, Noti.alert)

    app.use(router.routes())
}