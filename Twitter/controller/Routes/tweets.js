module.exports = (app, router) => {
    router.get(`/tweet`, tweet.list)
        .post(`/tweet`, tweet.create)
        .put(`/tweet/:id/like`, tweet.like)
        .delete(`/tweet/:id/like`, tweet.unlike)
        .post(`/tweet/:id/retweet`, tweet.retweet)
        .put(`/tweet/:id/voteid`, tweet.vote)
        .post(`/tweet/:id/reply`, tweet.reply)

    app.use(router.routes())
}