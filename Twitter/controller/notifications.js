const User = require('../repository/users')
const Notification = require('../repository/notification')

module.exports = {
    list,
    markAsRead
}

async function list(ctx, next) {
    let result = await Notification.list(ctx.pool, ctx.session.userId)
    console.log(result)
    await next()
}

async function markAsRead(ctx, next) {
    // await Notification.markAsRead(ctx.pool, notiId)
    ctx.body = "This Notification marked"
    await next()
}