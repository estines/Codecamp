const User = require('../repository/users')
const Noti = require('../repository/notification')

module.exports = {
    follow,
    unfollow
}

async function follow(ctx, next) {
    try {
        User.follow(ctx.pool, ctx.session.userId, ctx.params.id)
        let follower = await User.getUsername(ctx.pool , ctx.session.userId)
        let following = await User.getUsername(ctx.pool, ctx.params.id)
        Noti.create(ctx.pool, ctx.params.id ,
             { "title": "follow", "content": `${follower} now following you`, "photo" : 0 })
        console.log( `Follow ${following} Success!!` )
    }
    catch (err) {
        console.error(`Error: ${err}`)
    }
    finally {
        await next()
    }
}


async function unfollow(ctx, next) {
    try {
        User.unfollow(ctx.pool, ctx.session.userId, ctx.params.id)
        let follower = await User.getUsername(ctx.pool, ctx.session.userId)
        let following = await User.getUsername(ctx.pool, ctx.params.id)
        Noti.create(ctx.pool, ctx.params.id,
            { "title": "follow", "content": `${follower} now unfollowed`, "photo": 0 })
        console.log(`Unfollow ${following} Success!!`)
    }
    catch (err) {
        console.error(`Error: ${err}`)
    }
    finally {
        await next()
    }
}