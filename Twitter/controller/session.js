const { session } = require('../config/modules')
const Session = require('../repository/session')
const db = require('../lib/db')()

module.exports = (app) => {
    app.keys = ['twitter session']
    app.use(session({
        key: 'twitter',
        maxAge: 3 * 10e4,
        httpOnly: true,
        store: {
            async get(key, maxAge, { rolling }) {
                const sess = await Session.get(db, key)
                return JSON.parse(sess.history)
            },
            set(key, sess, maxAge, { rolling }) {
                Session.set(db, key, sess)
            },
            destroy(key) {
                Session.destroy(db, key)
            }
        }
    }, app))

    app.use(async (ctx, next) => {
        if (ctx.path === '/favicon.ico') return
        let count = ctx.session.views || 0
        ctx.session.userId
        ctx.session.views = ++count
        ctx.body = `${count} views`
        await next()
    })
}