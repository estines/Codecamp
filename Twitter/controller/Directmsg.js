const User = require('../repository/users')
const Noti = require('../repository/notification')

module.exports = {
    get,
    send,
    receive
}

async function get(ctx, next) {
    let messages = await User.getChat(ctx.pool, ctx.session.userId)
    console.log(messages)
    await next()
}

async function send(ctx, next) {
    const { content, type } = ctx.request.body
    User.sendChat(ctx.pool, ctx.session.userId, ctx.params.userid, content, type)
    let sender = await User.getUsername(ctx.pool, ctx.session.userId)
    let receiver = await User.getUsername(ctx.pool, ctx.params.userid)
    Noti.create(ctx.pool, ctx.params.userid, { "title": `${receiver} have message from ${sender}`, "content": content, "photo": 0 })
    ctx.body = "Sending Message Success!!"
    console.log("Send Message!!")
    await next()
}

async function receive(ctx, next) {
    let message = await User.receiveChat(ctx.pool, ctx.params.id)
    console.log(message)
    await next()
}