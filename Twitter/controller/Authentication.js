const { bcrypt } = require('../config/modules')
const User = require('../repository/users')
const Session = require('../repository/session')
module.exports = {
    signup,
    signin,
    signout,
    verify
}

async function signup(ctx, next) {
    let { user } = ctx.request.body
    user.password = await bcrypt.hash(user.password, 10)
    User.create(ctx.pool, user)
    ctx.body = "Signup Success!!"
    await next()
}

async function signin(ctx, next) {
    let hashed, username, email, check
    const { password } = ctx.request.body
    try {
        if (ctx.request.body.username) {
            username = ctx.request.body.username
            userInfo = await User.getUserInfo(ctx.pool, username, null)
        }
        else {
            email = ctx.request.body.email
            userInfo = await User.getUserInfo(ctx.pool, null, email)
        }
        check = await bcrypt.compare(password, userInfo.password)

        if (check) {
            console.log("-----------------login--------------------")
            ctx.body = "Signin Success!!"
            ctx.session.userId = userInfo.id
        }
        else {
            ctx.body = "Password is wrong!!, Try Again..."
        }
        await next()

    }
    catch (err) {
        console.error(`Error: ${err}`)
        ctx.body = "Username or email not found!!, Please Sign up before"
        await next()
    }
}

async function signout(ctx, next) {
    ctx.session.userId = null
    await next()
}

async function verify(ctx, next) {
    await next()
}