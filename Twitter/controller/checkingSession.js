module.exports = {
    checkSess
}

async function checkSess(ctx, next) {
    if( ctx.path === '/auth/signup' || ctx.path === '/auth/signin') {
        await next()
    }
    else {
        if(!ctx.session.userId) {
            ctx.body = "Unautherized"
            return
        }
        ctx.body = "Autherized"
        await next()
    }
}