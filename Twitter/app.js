const { koa, session } = require('./config/modules')
const Routes = require('./controller/routes')
const setSession = require('./controller/session')
const Session = require('./repository/session')
const app = new koa()
const db = require('./lib/db')()

app.use(createDB)
app.use(requestLogger)
setSession(app)
Routes(app)
app.use(Authenticate)
app.listen(3000)

async function createDB(ctx, next) {
    ctx.pool = require('./lib/db')()
    await next()
}

async function requestLogger(ctx, next) {
    await next()
    console.log(`${ctx.method} ${ctx.path}`)
}

async function Authenticate(ctx, next) {
    console.log("-------------Authentication-----------------")
    if (!ctx.session.userId) {
        ctx.body = "Unautherized"
        ctx.status = 401
        console.log("Unautherized")
    }
    else {
        ctx.body = "Autherized"
        console.log("Autherized")
    }
    await next()
}