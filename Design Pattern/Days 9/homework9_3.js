const _ = require('lodash')

class MyUtility {
    constructor() { }

    assign(object, source, callback) {
        return _.assign(object, source, callback)
    }

    keyBy(collection, iteratee) {
        return _.keyBy(collection, iteratee)
    }

    times(n, callback) {
        return _.times(n, callback)
    }

    cloneDeep(value, callback) {
        return _.cloneDeep(value, callback)
    }

    filter(collection, callback) {
        return _.filter(collection, callback)
    }

    sortBy(collection, callback) {
        return _.sortBy(collection, callback)
    }
}

let test = new MyUtility()
let arrays = [1, 2, 3, 7, 5, 4]
let log = test.filter(arrays, function (i) {
    return i%2
})
console.log(log)