let { Employee } = require('./employee.js')
let { Programmer } = require('./programmer.js')
let { OfficeCleaner } = require('./officecleaner.js')
let fs = require('fs')

class CEO extends Employee {
    constructor(firstname, lastname, salary, id, dressCode) {
        super(firstname, lastname, salary)
        this.id = id
        this.dressCode = dressCode
        this.employees = []
        this.employeesRaw
    }

    _readFile() {
        return new Promise((resolve, reject) => {
            fs.readFile('employee9.json', 'utf-8', (err, data) => {
                if (err) {
                    reject(err)
                }
                else {
                    resolve(data)
                }
            })
        })
    }

    _writeFile() {
        fs.writeFileSync('robot.txt', 'I\'m head.'+'\n'+ 'I\'m body.' + '\n' + 'I\'m leg.' + '\n' + 'I\'m feet.', 'utf-8')
    }

    createRobotFile() {
        return this._writeFile()
    }

    async setEmployees() {
        try {
            this.employeesRaw = JSON.parse(await this._readFile())
            this.employeesRaw.forEach(employee => {
                this._classifyEmployeeRole(employee, employee.role)
            })
            this.employees.forEach(employee => employee.work(this.employees[2]))
        }
        catch (error) {
            console.error(error)
        }
    }

    _classifyEmployeeRole(employee, role) {
        if (role == 'Programmer') {
            this._createProgrammer(employee)
        }
        else if (role == 'OfficeCleaner') {
            this._createOfficeCleaner(employee)
        }
        else if (role == 'CEO') {
            this._createCEO(employee)
        }
    }

    _createProgrammer(employee) {
        this.employees.push(new Programmer(employee.firstname,
            employee.lastname,
            employee.salary,
            employee.id,
            employee.type))
    }

    _createCEO(employee) {
        this.employees.push(new CEO(employee.firstname,
            employee.lastname,
            employee.salary,
            employee.id,
            employee.dressCode))
    }

    _createOfficeCleaner(employee) {
        this.employees.push(new OfficeCleaner(employee.firstname,
            employee.lastname,
            employee.salary,
            employee.id,
            employee.dressCode))
    }

    getSalary() {  // simulate public method
        return super.getSalary() * 2
    }
    work(employee) {  // simulate public method
        this._fire(employee)
        this._hire(employee)
        this._seminar()
        this._golf()
    }

    _fire(employee) {
        console.log(employee.firstname + ' has been fired! Dress with :' + this.dressCode)
    }

    _hire(employee) {
        console.log(employee.firstname + ' has been hired back! Dress with :' + this.dressCode)
    }

    _seminar() {
        console.log('he is going to seminar Dress with :' + this.dressCode)
    }

    increaseSalary(employee, newSalary) {
        if (newSalary > employee.getSalary()) {
            console.log(employee.firstname + '\'s salary has been set to ' + newSalary)
        }
        else {
            console.log(employee.firstname + '\'s salary is less than before!!')
        }
        return employee.setSalary(newSalary)
    }

    _golf() { // simulate private method
        this.dressCode = 'golf_dress'
        console.log("He goes to golf club to find a new connection." + " Dress with :" + this.dressCode)

    }

    reportRobot(self, robotMessage) {
        self.talk(robotMessage);
    }

    talk(message) {
        console.log(message)
    }
}

exports.CEO = CEO