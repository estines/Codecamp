let {Employee} = require('./employee.js')

class OfficeCleaner extends Employee{
    constructor(firstname, lastname, salary, id, dressCode) {
        super(firstname, lastname, salary)
        this.id = id
        this.dressCode = dressCode
    }

    work() {
        this._clean()
        this._KillCoachroach()
        this._DecorateRoom()
        this._WelcomeGuess()
    }

    _clean() {
        console.log('Clean')
    }

    _KillCoachroach() {
        console.log('KillCoachroach')
    }

    _DecorateRoom() {
        console.log('DecorateRoom')
    }

    _WelcomeGuess() {
        console.log('WelcomeGuess')
    }
}

exports.OfficeCleaner = OfficeCleaner