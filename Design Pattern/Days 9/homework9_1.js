const {CEO} = require('./ceo.js')
const fs = require('fs')
const EventEmitter = require('events')
const myEmitter = new EventEmitter()

let Peepo = new CEO('Peepo','Oreo',500000,9999,'Birthday Suit')

myEmitter.on('report', Peepo.reportRobot)

myEmitter.on('writeFile', () => {
    fs.access('robot.txt', fs.constants.R_OK, (err) => {
        if(err) {
            console.log('File not found, Now creating...')
            Peepo.createRobotFile('Hello World')
            myEmitter.emit('writeFile')
        }
        else {
            let content = fs.readFileSync('robot.txt','utf-8')
            myEmitter.emit('report', Peepo,content)
        }
    })
})

myEmitter.emit('writeFile')