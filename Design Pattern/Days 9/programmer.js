let {Employee} = require('./employee')

class Programmer extends Employee {
    constructor(firstname, lastname, salary, id, type) {
        super(firstname, lastname, salary)
        this._id = id
        this._type = type
    }

    work() {
        this._CreateWebsite()
        this._FixPC()
        this._InstallWindows()
    }

    _CreateWebsite() {
        console.log(this.firstname + ' is creating website')
    }

    _FixPC() {
        console.log(this.firstname + ' is Fixing pc!')
    }

    _InstallWindows() {
        console.log(this.firstname + ' is installing windows!')
    }

}

exports.Programmer = Programmer