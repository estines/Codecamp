const mysql = require('mysql2/promise')
const {config} = require('../config/database.js')

class Database {
    constructor(){
        this.connectDB()
    }

    Query() {
        return this.connection.execute('SELECT * FROM user');
    }

    async connectDB() {
        this.connection = await mysql.createConnection({
            host: config.host,
            user: config.user,
            password: config.password,
            database: config.database
        })
    }
}

module.exports = () => {
    return new Database()
}