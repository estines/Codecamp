const {Query} = require('../models/homework10_1.js')

module.exports = (app) => {
    app.use(async (ctx, next) => {
        try {
            let [rows,fields] = await Query()
            await ctx.render('user', { "keys": Object.keys(rows[0]), "values": rows })
            await next()
        } catch (err) {
            ctx.status = 400
            ctx.body = `Uh-oh: ${err.message}`
        }
    })
}