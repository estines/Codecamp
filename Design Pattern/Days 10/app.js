const Koa = require('koa') , app = new Koa() , render = require('koa-ejs'), path = require('path')

render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false,
    debug: true
})

require('./controller/routes.js')(app)

app.listen(3000)