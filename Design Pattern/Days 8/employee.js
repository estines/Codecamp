class Employee {
    constructor(firstname, lastname, salary) {
        this.firstname = firstname
        this.lastname = lastname
        this._salary = salary; // simulate private variable

    }
    setSalary(newSalary) { // simulate public method
        if (newSalary > this._salary) {
            this._salary = newSalary
            return newSalary
        }
        else {
            return false            
        }

        // return newSalary ถ้ามีเงินเดือนใหม่มีค่ามากกว่า this._salary
        // return false ถ้าเงินเดือนใหม่มีค่าน้อยกว่าเท่ากับ this._salary
        
    }
    getSalary () {  // simulate public method
        return this._salary;
    };
    work(employee) {
        // leave blank for child class to be overiddens
    }
    leaveForVacation(year, month, day) {

    }
    gossip(employee, message) {
        console.log('Hey '+ employee.firstname + ', '+ message)
    }
}

exports.Employee = Employee