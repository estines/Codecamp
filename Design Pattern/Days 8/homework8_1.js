let {CEO} = require('./ceo.js')
let {Employee} = require('./employee.js')
let somchai = new CEO("Somchai","Sudlor",30000);
let somsri = new Employee("Somsri","Sudsuay",22000);

// ข้อ 1
somchai.gossip(somsri,"Today is very cold!");
somchai.work(somsri);

somchai.increaseSalary(somsri, 200000);
somchai.increaseSalary(somsri, 25000);

