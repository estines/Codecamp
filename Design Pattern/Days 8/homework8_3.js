class MobilePhone {
    constructor(color) {
        this.color = color
    }

    PhoneCall() {
        // simulate method
    }

    SMS() {
        // simulate method
    }

    InternetSurfing() {
        // simulate method
    }
}

class Samsung extends MobilePhone {

    UseGearVR() {
        // simulate method
    }

    TransformToPC() {
        // simulate method
    }
    
    GooglePlay() {

    }

}

class Apple extends MobilePhone {
    
    appStore() {

    }
}


class SamsungGalaxyS8 extends Samsung {
    constructor(color) {
        super(color)
    }

}

class SamsungGalaxyNote8 extends Samsung {
    constructor(color) {
        super(color)
    }

    UsePen() {
        // simulate method
    }
}

class iPhone8 extends Apple {
    constructor(color) {
        super(color)
    }

    _TouchID() {
        // simulate method
    }
}

class iPhoneX extends Apple {
    constructor(color) {
        super(color)
    }

    _FaceID() {
        // simulate method
    }
}