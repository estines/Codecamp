let {Employee} = require('./employee.js')
let {Programmer} = require('./programmer.js')
let fs = require('fs')

class CEO extends Employee {
    constructor(firstname, lastname, salary) {
        super(firstname, lastname, salary)
        this.dressCode = 'suit'
        this.employees = []
        this.employeesRaw
    }

    _readFile() {
        return new Promise ( (resolve, reject) => {
            fs.readFile('homework1.json','utf-8', (err,data) => {
                if (err) {
                    reject(err)
                }
                else {
                    resolve(data)
                }
            })
        })
    }

    async setProgrammers() {
        try {
            this.employeesRaw = JSON.parse(await this._readFile())
            this._createProgrammers(this.employeesRaw)
            console.log(this.employees)
        }
        catch (error) {
            console.error(error)
        }
    }

    _createProgrammers(employees) {
        employees.forEach( employee => {
            this.employees.push(new Programmer(employee.firstname, employee.lastname, employee.salary, employee.id, "Full Stack"))
        })
    }

    getSalary(){  // simulate public method
        return super.getSalary()*2
    }
    work (employee) {  // simulate public method
        this._fire(employee)
        this._hire(employee)
        this._seminar()
        this._golf()
    }

    _fire(employee) {
        employee.dressCode = 'tshirt'
        console.log(employee.firstname + ' has been fired! Dress with :' + employee.dressCode)
    }

    _hire(employee) {
        employee.dressCode = 'tshirt'
        console.log(employee.firstname + ' has been hired back! Dress with :' + employee.dressCode)
    }

    _seminar() {
        console.log('he is going to seminar Dress with :'+ this.dressCode)
    }

    increaseSalary(employee, newSalary) {
        if (newSalary > employee.getSalary()) {
            console.log(employee.firstname + '\'s salary has been set to ' + newSalary)
        }
        else {
            console.log(employee.firstname + '\'s salary is less than before!!')
        }
        return employee.setSalary(newSalary)
    }

    _golf () { // simulate private method
        this.dressCode = 'golf_dress'
        console.log("He goes to golf club to find a new connection." + " Dress with :" + this.dressCode)
        
    }
}

exports.CEO = CEO