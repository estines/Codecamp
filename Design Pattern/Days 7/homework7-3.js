function cloneObjects(object) {
    let newObj
    if (Array.isArray(object) == true) {
        newObj = []
        object.forEach(item => {
            if(Array.isArray(item) == true){
                let obj = []
                item.forEach( subitem => {
                    let subObj
                    if (Array.isArray(subitem) == true) {
                        subObj = []
                        subitem.forEach( unit => {
                            subObj.push(unit)
                        })
                    }
                    else if (typeof(subitem) == 'object') {
                        subObj = {}
                        let subitemKeys = Object.keys(item[key])
                        subitemKeys.forEach(subkey => subObj[subkey] = item[key][subkey])
                    }
                    else {
                        obj.push(subitem)
                    }
                    obj.push(subObj)
                })
                newObj.push(obj)
            }
            else if (typeof(item) == 'object') {
                let obj = {}
                let itemkeys = Object.keys(item)
                itemkeys.forEach(key => {
                    let subObj
                    if (Array.isArray(item[key]) == true) {
                        subObj = []
                        item[key].forEach ( subitem => {
                            subObj.push(subitem)
                        })
                    }
                    else if (typeof (item[key]) == 'object') {
                        subObj = {}
                        let subitemKeys = Object.keys(item[key])
                        subitemKeys.forEach( subkey => subObj[subkey] = item[key][subkey] )
                    }
                    else {
                        subObj = {}
                        subObj = item[key]
                    }
                    obj[key] = subObj
                })
                newObj.push(obj)
            }
            else {
                newObj.push(item)
            }
        })
        return newObj
    }
    else {
        newObj = {}
        for(let key in object){
            if (Array.isArray(object[key]) == true) {
                let obj = []
                object[key].forEach( item => {
                    let subObj
                    if (Array.isArray(item) == true) {
                        subObj = []
                        item.forEach(subitem => {
                            subObj.push(subitem)
                        })
                        obj.push(subObj)
                    }
                    else if (typeof(item) == 'object') {
                        subObj = {}
                        let itemkeys = Object.keys(item)
                        itemkeys.forEach( key => {
                            subObj[key] = item[key]
                        })
                        obj.push(subObj)
                    }
                    else {
                        obj.push(item)
                    }
                })
                newObj[key] = obj
            }
            else if (typeof (object[key]) == 'object') {
                let obj = {}
                let itemkeys = Object.keys(object[key])
                itemkeys.forEach( subkey => {
                    let subObj
                    if (Array.isArray(object[key][subkey]) == true) {
                        subObj = []
                        object[key][subkey].forEach ( item => {
                            subObj.push(item)
                        })
                        obj[subkey] = subObj
                    }
                    else if (typeof(object[key][subkey]) == 'object') {
                        subObj = {}
                        let subitemKeys = Object.keys(object[key][subkey])
                        subitemKeys.forEach ( unit => {
                            subObj[unit] = object[key][subkey][unit]
                        })
                        obj[subkey] = subObj
                    }
                    else {
                        obj[subkey] = object[key][subkey]
                    }
                })
                newObj[key] = obj
            }
            else {
                newObj[key] = object[key]
            }
        }
        return newObj
    }
}

let arrays = [1, 2, { a: 1, b: [ 3, 4 ] ,c: { d: 5, e:6} }]
let objects = { key1: [1,2, {a:1,b:2}, 3], key2: {key1s:1}, key3: [5,[2,3],6], key4: {test1:'hello', test2: {hi:'object',bye: 'mybrain'}} }
let newObjects = cloneObjects(objects)

// Objects Test
newObjects['key1'][2]['b'] = 10
console.log(newObjects['key1'][2]['b'],objects['key1'][2]['b'])

// Arrays Test
newObjects[2]['c']['d'] = 0
console.log(newObjects[2]['c']['d'], arrays[2]['c']['d'])