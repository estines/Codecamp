const koa = require('koa')
const app = new koa();
const logger = require('koa-logger')
const fs = require('fs')
const winston = require('winston')

const winLog = winston.createLogger({
    level: 'info',
    format: winston.format.json(),
    transports: [
        new winston.transports.File({ filename: 'error.log', level: 'error' }),
        new winston.transports.File({ filename: 'combined.log' })
    ]
})
winLog.add(new winston.transports.Console({
    format: winston.format.simple()
}))

function readFiles(filename) {
    return new Promise ( (resolve,reject) => {
        fs.readFile(filename, 'utf-8', (err, data) => {
            if (err) reject(err)
            else resolve(data)
        })
    })
}

app.use(async (ctx, next) => {
    ctx.body = await readFiles('index.html')
    await next()
})

app.listen(3000)
app.use(logger())

winLog.info('This is info log');
winLog.error('This is error log');