const fs = require('fs')
const files = {json: 'homework1.json'}

function readFiles(filename) {
    return new Promise ( (resolve, reject) => {
        fs.readFile(filename,'utf-8', (err,data) => {
            if (err) reject(err)
            else resolve(JSON.parse(data))
        })
    })
}

function addYearSalary(row) {
    row['yearSalary'] = row.salary*12
    return row

}

function addNextSalary(row) {
    row['nextSalary'] = [Math.round(row.salary), Math.round(row.salary * 1.1), Math.round((row.salary * 1.1)*1.1)]
    return row
}

function addAdditionFields(employees) {
    employees.forEach( person => {
        addYearSalary(person)
        addNextSalary(person)
    })
    return employees
}

async function executeFiles(keyword) {
    let employees = await readFiles(keyword)
    addAdditionFields(employees)
    console.log(employees)
}

executeFiles(files['json'])