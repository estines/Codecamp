let fs = require('fs')
let filename = { json: 'homework1-4.json', eyes: 'homework5-1_eyes.json'}


function readFiles(keyword) {
    return new Promise ( (reject,resolve) => {
        fs.readFile(filename[keyword],'utf-8', (err, object) => {
            if (err) reject(err)
            else resolve(object)
        })
    })
}

function eyeClassify (object, initial = {male: 0, female: 0}) {
    let maleCount = { brown: 0, blue: 0, green: 0}, femaleCount = { brown: 0, blue: 0, green: 0}
    object.forEach( person => {
        if (person.gender == 'male') {
            
        }
        else if (person.gender == 'female') {
            
        }
    })   
    return initial
}

async function executeFiles(keyword) {
    try {
        let Object = await readFiles(keyword)
        console.log(Object)
    }
    catch (error) {
        console.error(error)
    }
}

executeFiles('json')