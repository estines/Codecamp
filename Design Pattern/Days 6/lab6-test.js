let sinon = require('sinon')
let fs = require('fs')
let assert = require('assert')
let { saveUserDatabase } = require('./lab6.js')
let spy = sinon.spy();

describe('SINON Lab', () => {
    /*describe('#spies test', () => {
        it('should write JSON string into output6_1.txt. check with spy', function () {
            const save = sinon.spy(fs, "writeFile")
            const user = {
                "firstname": "Somchai",
                "lastname": "Sudlor"
            }
            let dummyCallbackFunction = err => { }
            saveUserDatabase(JSON.stringify(user), "output6_1.txt", dummyCallbackFunction);
            save.restore()
            sinon.assert.calledOnce(save)
            sinon.assert.calledWith(save, 'output6_1.txt')
        })
    })

    describe('#dummy test', () => {
        it('should write JSON string into output6_1.txt. check with spy', () => {
            const save = sinon.spy(fs, "writeFile")
            const user = {
                "firstname": "Somchai",
                "lastname": "Sudlor"
            }
            let dummyCallbackFunction = function (err) { }
            saveUserDatabase(user, "output6_1.txt", dummyCallbackFunction)
            save.restore()
            sinon.assert.calledOnce(save)
            sinon.assert.calledWith(save, 'output6_1.txt')
        })
    })*/

    describe('#stub test', () => {
        it('should write JSON string into output6_1.txt', () => {
            const save = sinon.stub(fs, 'writeFile')
            save.yields(null)
            const callbackFunction = sinon.spy()
            const user = {
                "firstname": "Somchai",
                "lastname": "Sudlor
            }
            saveUserDatabase(user, "output6_1.txt", callbackFunction)
            save.restore()
            sinon.assert.calledOnce(save)
            sinon.assert.calledWith(callbackFunction, null)
        })
    })

    /*
    describe('#mock test', () => {
        it('should write JSON string into output6_1.txt. check with mock', () => {
            const user = {
                "firstname": "Somchai",
                "lastname": "Sudlor"
            }
            var mockFs = sinon.mock(fs)
            mockFs.expects('writeFile').once().withArgs("output6_1.txt")
            saveUserDatabase(user, "output6_1.txt", function (err) { })

            mockFs.verify()
            mockFs.restore()
        })
    })*/
})