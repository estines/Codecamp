const bookstore = require('../model/bookstore')

module.exports = {
    list, create, update, remove
}

async function list(ctx) {
    ctx.body = {}
}

async function create(ctx) {
    let { id, name, price, image } = ctx.request.body
    ctx.body = await bookstore.create(ctx.pool, id, name, price, image)
}

async function update(ctx) {
    let { id, name, price, image } = ctx.request.body
    ctx.body = await bookstore.update(ctx.pool, id, name, price, image)
}

async function remove(ctx) {
    let { id } = ctx.request.body
    ctx.body = await bookstore.remove(ctx.pool, id)
}