const Router = require('koa-router')
const bookstore = require('./bookstore')

router = new Router()
    .get('/products', bookstore.list)
    .post('/products', bookstore.create)
    .patch('/products/:id', bookstore.update)
    .delete('/products/:id', bookstore.remove)

module.exports = app => {
    app.use(router.routes())
}