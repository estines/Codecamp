module.exports = {
    list, create, update, remove, search
}

async function list(db) {
    let [result] = await db.execute(`select * from books_list`)
}

async function create(db, isbn, title, price, imgUrl) {
    try {
        await db.execute(`insert into books_list(ISBN,Title,Price,imageUrl) 
        values(?,?,?,?)`, [isbn, title, price, imgUrl])
        return ture
    }
    catch (err) {
        return false
    }

}

async function update(db, isbn, title, price, imgUrl) {
    await db.execute(`update books_list set Title=?, Price=?, imageUrl=? where ISBN = ?`,
        [title, price, imgUrl, isbn])
}

async function remove(db, isbn) {
    await db.execute(`delete from books_list where isbn = ?`, [isbn])
}

async function search(db, keyword) {
    let [result] = await db.execute(`select * from books_list where title like %?% or isbn like %?% `,
        [keyword, keyword])
}