const koa = require('koa')
const cors = require('koa2-cors')
const bodyparser = require('koa-bodyparser')
const Routes = require('./controller/routes')
const app = new koa()
app.use(requestLogger)
app.use(createPool)
app.use(cors())
app.use(bodyparser())
Routes(app)
app.listen(3000)

async function requestLogger(ctx, next) {
    await next()
    console.log(`${ctx.method} ${ctx.path}`)
}

async function createPool(ctx, next) {
    ctx.pool = require('./lib/db')()
    await next()
}