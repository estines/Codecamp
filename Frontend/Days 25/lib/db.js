const mysql = require('mysql2/promise')

module.exports = () => {
    return mysql.createPool({
        host: 'localhost',
        user: 'root',
        password: 'root',
        database: 'bookstore'
    })
}