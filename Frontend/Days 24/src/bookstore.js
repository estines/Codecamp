export class bookstore {
    constructor() {
    }

    static receiveBooksDetail(data) {
        data.image = this._bookstorage()
        this._createBooks(data)
    }

    static editBooksDetail(data) {
        data.image = this._bookstorage()
        this.editBooks(data)
    }

    static _bookstorage(image) {
        this.books = [
            { isbn: "1001", title: "LINEBot with PHP", price: "15.99", imageUrl: "http://203.170.160.50/naiinpann/Ebook/27362/Thumbnail/middle.gif" },
            { isbn: "1002", title: "Reskin Game in A Day", price: "29", imageUrl: "http://203.170.160.50/naiinpann/Ebook/27267/Thumbnail/middle.gif" },
            { isbn: "1003", title: "Ragnarok:The Legend", price: "19.99", imageUrl: "http://203.170.160.50/naiinpann/Ebook/20927/Thumbnail/middle.gif" },
            { isbn: "1004", title: "Apple Watch", price: "9.49", imageUrl: "http://203.170.160.50/naiinpann/Ebook/20585/Thumbnail/middle.gif" }
        ]
        let cover = []
        this.books.map(book => {
            Object.keys(book).map(key => {
                if (key === "imageUrl") return cover.push(book[key])
            })
        })

        return cover[Math.floor(Math.random() * cover.length)]
    }

    static _createBooks(data) {
        $("#bookslist").append(`
            <div class="col-md-3" id=${data.id}>
                <div class="books border border-mute rounded">
                    <h5 class="text-center" style="margin-top:15px;">${data.name}</h5>
                    <center>
                        <img src= ${data.image} width="85%">
                    </center>
                    <p class="text-center" style="margin-top:10px;">Price: ${data.price}$&nbsp;&nbsp;
                        <button class="btn btn-primary edit-btn" bookid=${data.id} data-toggle="modal" data-target="#edit" >Edit</button>
                        <button class="btn btn-danger remove-btn" bookid="${data.id}" data-toggle="modal" data-target="#alert">
                                    <i class="fa fa-times"></i>
                                </button>
                    </p>
                </div>
            </div>
        `)
    }

    static editBooks(data) {
        $(`#${data.id} div h5`).html(`${data.name}`)
        $(`#${data.id} div center img`).attr("src", `${data.image}`)
        $(`#${data.id} div p`).html(`
            Price: ${data.price}$&nbsp;&nbsp;
            <button class="btn btn-primary edit-btn" bookid=${data.id} data-toggle="modal" data-target="#edit">Edit</button>
            <button class="btn btn-danger remove-btn" bookid="${data.id}" data-toggle="modal" data-target="#alert">
                    <i class="fa fa-times"></i>
            </button>
        `)
    }
}