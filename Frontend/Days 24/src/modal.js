export class modal {
    static getModalValue() {
        if (!$("#book-id").val() || !$("#book-name").val() || !$("#book-price").val() 
            || !$("#book-date").val() || !$("#book-img").val()) return
        let data = {
            id: $("#book-id").val(),
            name: $("#book-name").val(),
            price: $("#book-price").val(),
            date: $("#book-date").val(),
            // Thu Jan 25 2018 15:45:56 GMT+0700 (+07)
            image: $("#book-img").val()
        }
        return data
    }

    static getEditValue() {
        if (!$("#edit-name").val() || !$("#edit-price").val()
            || !$("#edit-date").val() || !$("#edit-img").val()) return
        let data = {
            id: $("#edit-id").attr("placeholder"),
            name: $("#edit-name").val(),
            price: $("#edit-price").val(),
            date: $("#edit-date").val(),
            image: $("#edit-img").val()
        }
        return data
    }
}