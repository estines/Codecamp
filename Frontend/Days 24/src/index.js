import { modal } from './modal'
import { bookstore } from './bookstore'


$(document).ready(() => {
    $("#save-btn").click(function () {
        try {
            let bookDetail = modal.getModalValue()
            alert(JSON.stringify(bookDetail))
            const url = 'http://localhost:3000/products';

            let fetchData = {
                method: 'POST',
                body: JSON.stringify(bookDetail),
                headers: new Headers({
                    'Content-Type': 'application/json'
                })
            }
            fetch(url, fetchData)
                .then(res => {
                })

            bookstore.receiveBooksDetail(bookDetail)
            alert("Insert book success!!")
        }
        catch (err) {
            alert("Information not complete!!")
        }
        finally {
            $("#add").modal('hide')
            $('#add form :input').val("")
        }
    })

    $("#edit-btn").click(function () {
        try {
            let bookDetail = modal.getEditValue()
            const url = `http://localhost:3000/products/${bookDetail.id}`;

            let fetchData = {
                method: 'PATCH',
                body: JSON.stringify(bookDetail),
                headers: new Headers({
                    'Content-Type': 'application/json'
                })
            }
            fetch(url, fetchData)
                .then(res => {
                    bookstore.editBooksDetail(bookDetail)
                })

            alert("Edit book success!!")
        }
        catch (err) {
            alert("Information not complete!!")
        }
        finally {
            $("#edit").modal('hide')
            $('#edit form :input').val("")
        }
    })

    $('body').on('click', '.edit-btn', function () {
        let id = $(this).attr("bookid")
        $('#edit #edit-id').attr("placeholder", id)
    })

    $("body").on('click', '.remove-btn', function () {
        let isbn = $(this).attr("bookid")
        let id = { id: isbn }
        $('#del-msg').html(`Are you sure to delete Book ISBN:${isbn} ?`)
        $('#delete-btn').click(function () {
            const url = `http://localhost:3000/products/${id}`;

            let fetchData = {
                method: 'DELETE',
                body: JSON.stringify(id),
                headers: new Headers({
                    'Content-Type': 'application/json'
                })
            }
            fetch(url, fetchData)
                .then(res => {
                    $(`#${isbn}`).remove()
                })

            $(`#alert`).modal('hide')
        })
    })
})