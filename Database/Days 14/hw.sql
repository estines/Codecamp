SELECT s.id, s.name, e.course_id AS Total_course, c.price AS Total_Price ,
    c.name AS Course_name, c.price AS Expensive_Course_Price
FROM students AS s
    INNER JOIN enrolls AS e ON e.student_id = s.id
    INNER JOIN courses AS c ON c.id = e.course_id
    INNER JOIN (SELECT students.id , students.name , MAX(courses.price) as price
    FROM students
        INNER JOIN enrolls ON enrolls.student_id = students.id
        INNER JOIN courses ON courses.id = enrolls.course_id
    GROUP BY students.id, students.name) AS f ON f.id = s.id
WHERE c.price = f.price