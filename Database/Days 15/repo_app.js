const User = require('./repository/user');

( async() => {    
    const db = await require('./lib/db')()
    let user1 = await User.find(db, 6)
    user1.firstName = 'Peepo'
    await User.store(db, user1)
    const user2 = await User.find(db,2)
    //User.remove(db, user2.id)
    const user3 = await User.findAll(db)
    //console.log(user3)
    const user4 = await User.findByUsername(db,'Peepo')
    console.log(user4)
})()