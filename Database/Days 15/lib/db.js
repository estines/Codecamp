const { modules_path } = require('../config/modules')
const { config } = require('../config/database')
const { mysql } = require(modules_path)

module.exports = async () => {
    return await mysql.createConnection({
        host: config.host,
        user: config.user,
        password: config.password,
        database: config.database
    })
}