function createEntity(row) {
    return {
        id: row.id,
        firstName: row.first_name,
        lastName: row.last_name
    }
}

async function find(db, id) {
    try {
        const [rows] = await db.execute(`select id, first_name, last_name from users where id = ? `, [id])
        return createEntity(rows[0])
    }
    catch (err) {
        console.error('Not found id = ' + id + ' in users')
        return { firstName: 'New', lastName: 'User' }
    }
}

async function findAll(db) {
    const [rows] = await db.execute(` select id, first_name, last_name from users `)
    return rows.map( row => createEntity(row))
}

async function store(db, user) {
    if (!user.id) {
        let [[last_id]] = await db.execute(`select id from users where id > 0 order by id desc limit 1`)
        let id = last_id['id'] + 1
        const result = await db.execute(` insert into users ( id, first_name, last_name ) values ( ?, ?, ? )`, [id, user.firstName, user.lastName])
        return
    }
    return db.execute(` update users set first_name = ?, last_name = ? where id = ?`, [user.firstName, user.lastName, user.id])
}

function remove(db, id) {
    return db.execute(` delete from users where id = ? `, [id])
}

async function findByUsername(db, username) {
    const [rows] = await db.execute(`select id,first_name, last_name from users where first_name = ?`, [username])
    return rows.map( row => createEntity(row) )
}

module.exports = {
    find, findAll, findByUsername, store, remove
}