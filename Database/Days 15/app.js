const mysql = require('mysql2/promise'), { config } = require('./config/database');
(async function () {
    const db = await require('./lib/db')()
    const User = require('./model/user')(db)
    const user1 = await User.find(10)
    user1.firstName = 'Peepo'
    await user1.save()
    const user2 = await User.find(6)
    await user2.remove()
    const user3 = await User.findAll()
    const user4 = await User.findByUsername('Peepo')
})()
    .then(() => { }, (err) => { console.error(`cant' remove`) })