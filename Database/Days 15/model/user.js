const mysql = require('mysql2/promise')

class User {
    constructor(db, row) {
        this._db = db
        this.id = row.id
        this.firstName = row.first_name
        this.lastName = row.last_name
    }

    async save() {
        if (!this.id) {
            let[[last_id]] = await this._db.execute(`select id from users where id > 0 order by id desc limit 1`)
            this.id = last_id['id'] + 1
            const result = await this._db.execute(`insert into users (id, first_name, last_name ) values (?, ?, ?) `, [this.id, this.firstName, this.lastName])
            return
        }
        return this._db.execute(` update users set first_name = ?,last_name = ? where id = ? `, [this.firstName, this.lastName, this.id])
    }

    
    remove() {
        return this._db.execute(`delete from users where id = ?`, [this.id])
    }
}

module.exports = function (db) {
    return {
        async find(id) {
            try {
                const [rows] = await db.execute(`select id,first_name, last_name from users where id = ? `, [id])
                return new User(db, rows[0])
            }
            catch (err) {
                const row = { first_name: 'Sawaddee', last_name: 'Newuser' }
                return new User(db, row)
            }
        },
        async findAll() {
            const [rows] = await db.execute(`select id,first_name, last_name from users `)
            return rows.map((row) => new User(db, row))
        },
        async findByUsername(username) {
            const [rows] = await db.execute(`select id,first_name, last_name from users where first_name = ?`, [username])
            return rows[0]
        }
    }
}

// Insert id that now blank
/* let [current_id] = await this._db.execute(`select id from users`)
this.i = 1
for (let id in current_id) {
    if (this.i != current_id[id]['id']) {
        break;
    }
    this.i++
}
console.log(this.i) */