-- 1)
SELECT * FROM departments

-- 2)
select COUNT(emp_no) as Total_employee , COUNT(case when gender = 'M' then 0 end) as Total_Male , COUNT(case when gender = 'F' then 0 END) as Total_Female
from employees

-- 3)
SELECT COUNT(DISTINCT last_name) AS Total_lastname FROM employees