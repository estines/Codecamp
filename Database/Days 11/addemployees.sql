INSERT INTO employees (id,firstname,lastname,age,added)
VALUES('1','Teemo','Onduty','27',NOW());

INSERT INTO employees (id,firstname,lastname,age,added)
VALUES('2','Chamod','Chedkae','33',NOW());

INSERT INTO employees (id,firstname,lastname,age,added)
VALUES('3','Corki','Isdog','26',NOW());

INSERT INTO employees (id,firstname,lastname,age,added)
VALUES('4','Jitra','Parduk','24',NOW());

INSERT INTO employees (id,firstname,lastname,age,added)
VALUES('5','Poesa','Toto','27',NOW());

INSERT INTO employees (id,firstname,lastname,age,added)
VALUES('6','Baboo','Bababa','27',NOW());

INSERT INTO employees (id,firstname,lastname,age,added)
VALUES('7','Goku','Shun','25',NOW());

INSERT INTO employees (id,firstname,lastname,age,added)
VALUES('8','Peepo','Seedang','19',NOW());

INSERT INTO employees (id,firstname,lastname,age,added)
VALUES('9','Pakapong','Roza','21',NOW());

INSERT INTO employees (id,firstname,lastname,age,added)
VALUES('10','Rojer','Siryessir','18',NOW());

DELETE FROM employees
WHERE id = 5;

ALTER TABLE employees
add column address varchar(255);

UPDATE employees
set address = '213'
WHERE id = 1;

UPDATE employees
set address = 'chaingmai'
WHERE id = 1;

UPDATE employees
set address = 'bangkok'
WHERE id = 1;

SELECT COUNT(*) FROM employees

SELECT firstname FROM employees
WHERE age < 20;