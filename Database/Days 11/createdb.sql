CREATE DATABASE Bookstore;

CREATE TABLE employees (
    id int,
    firstname varchar(255),
    lastname varchar(255),
    age int,
    added DATETIME,
    PRIMARY KEY (id)
);

CREATE TABLE books (
    ISBN varchar(13),
    book_name varchar(255),
    book_price int,
    added DATETIME,
    PRIMARY KEY (ISBN)
);

CREATE TABLE pricelist (
    ISBN varchar(13),
    sold_by varchar(255),
    book_price int,
    piece int,
    added DATETIME
);