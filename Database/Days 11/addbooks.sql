INSERT INTO books (ISBN,book_name,book_price,added)
VALUES('0000000000001','Javascript','249',NOW());

INSERT INTO books (ISBN,book_name,book_price,added)
VALUES('0000000000002','Pokemon Go','79',NOW());

INSERT INTO books (ISBN,book_name,book_price,added)
VALUES('0000000000003','Italia Foods','469',NOW());

INSERT INTO books (ISBN,book_name,book_price,added)
VALUES('0000000000004','League of Legend','199',NOW());

INSERT INTO books (ISBN,book_name,book_price,added)
VALUES('0000000000005','Advance Javascript','329',NOW());

INSERT INTO books (ISBN,book_name,book_price,added)
VALUES('0000000000006','Titanic','549',NOW());

INSERT INTO books (ISBN,book_name,book_price,added)
VALUES('0000000000007','Angle&Satan','315',NOW());

INSERT INTO books (ISBN,book_name,book_price,added)
VALUES('0000000000008','Harry Potter','890',NOW());

INSERT INTO books (ISBN,book_name,book_price,added)
VALUES('0000000000009','Calculus I','299',NOW());

INSERT INTO books (ISBN,book_name,book_price,added)
VALUES('0000000000010','Calculus II','299',NOW());

SELECT * FROM books
WHERE book_name like '%Script%'

SELECT * FROM books
WHERE book_name like '%a%'
LIMIT 4;

INSERT INTO pricelist (ISBN, sold_by, book_price, piece, added) VALUES
('0000000000001','Baboo','299','2',NOW()),
('0000000000003','Poesa','529','1',NOW()),
('0000000000004','Goku','229','11',NOW()),
('0000000000007','Poesa','369','1',NOW()),
('0000000000002','Rojer','99','2',NOW()),
('0000000000006','Baboo','699','1',NOW()),
('0000000000006','Baboo','699','1',NOW()),
('0000000000001','Goku','299','1',NOW()),
('0000000000005','Poesa','389','3',NOW()),
('0000000000002','Rojer','99','2',NOW()),
('0000000000007','Rojer','369','6',NOW()),
('0000000000002','Peepo','99','1',NOW()),
('0000000000001','Peepo','299','1',NOW()),
('0000000000005','Rojer','389','2',NOW()),
('0000000000008','Baboo','999','4',NOW()),
('0000000000007','Poesa','369','6',NOW()),
('0000000000005','Baboo','389','1',NOW()),
('0000000000003','Rojer','529','1',NOW()),
('0000000000003','Baboo','529','1',NOW()),
('0000000000003','Poesa','529','3',NOW());

SELECT SUM(piece)
FROM pricelist

SELECT DISTINCT ISBN
FROM pricelist
WHERE piece != 0

SELECT SUM(book_price)
FROM pricelist

-- INSERT INTO pricelist (ISBN, sold_by, book_price, piece, added)
-- VALUES('0000000000003','Poesa','529','1',NOW());

-- INSERT INTO pricelist (ISBN, sold_by, book_price, piece, added)
-- VALUES('0000000000004','Goku','229','11',NOW());

-- INSERT INTO pricelist (ISBN, sold_by, book_price, piece, added)
-- VALUES('0000000000007','Poesa','369','1',NOW());

-- INSERT INTO pricelist (ISBN, sold_by, book_price, piece, added)
-- VALUES('0000000000002','Rojer','99','2',NOW());

-- INSERT INTO pricelist (ISBN, sold_by, book_price, piece, added)
-- VALUES('0000000000006','Baboo','699','1',NOW());

-- INSERT INTO pricelist (ISBN, sold_by, book_price, piece, added)
-- VALUES('0000000000001','Goku','299','1',NOW());

-- INSERT INTO pricelist (ISBN, sold_by, book_price, piece, added)
-- VALUES('0000000000005','Poesa','389','3',NOW());

-- INSERT INTO pricelist (ISBN, sold_by, book_price, piece, added)
-- VALUES('0000000000002','Rojer','99','2',NOW());

-- INSERT INTO pricelist (ISBN, sold_by, book_price, piece, added)
-- VALUES('0000000000010','Baboo','319','1',NOW());

-- INSERT INTO pricelist (ISBN, sold_by, book_price, piece, added)
-- VALUES('0000000000007','Rojer','369','6',NOW());

-- INSERT INTO pricelist (ISBN, sold_by, book_price, piece, added)
-- VALUES('0000000000002','Peepo','99','1',NOW());

-- -- INSERT INTO pricelist (ISBN, sold_by, book_price, piece, added)
-- -- VALUES('0000000000001','Peepo','299','1',NOW());

-- -- INSERT INTO pricelist (ISBN, sold_by, book_price, piece, added)
-- -- VALUES('0000000000005','Rojer','389','2',NOW());

-- INSERT INTO pricelist (ISBN, sold_by, book_price, piece, added)
-- VALUES('0000000000008','Baboo','999','4',NOW());

-- INSERT INTO pricelist (ISBN, sold_by, book_price, piece, added)
-- VALUES('0000000000007','Poesa','369','6',NOW());

-- INSERT INTO pricelist (ISBN, sold_by, book_price, piece, added)
-- VALUES('0000000000006','Rojer','699','2',NOW());

-- INSERT INTO pricelist (ISBN, sold_by, book_price, piece, added)
-- VALUES('0000000000005','Baboo','389','1',NOW());

-- INSERT INTO pricelist (ISBN, sold_by, book_price, piece, added)
-- VALUES('0000000000003','Rojer','529','1',NOW());

-- INSERT INTO pricelist (ISBN, sold_by, book_price, piece, added)
-- VALUES('0000000000003','Baboo','529','1',NOW());

-- INSERT INTO pricelist (ISBN, sold_by, book_price, piece, added)
-- VALUES('0000000000003','Poesa','529','3',NOW());

-- INSERT INTO pricelist (ISBN, sold_by, book_price, piece, added)
-- VALUES('0000000000002','Corki','99','7',NOW());

-- INSERT INTO pricelist (ISBN, sold_by, book_price, piece, added)
-- VALUES('0000000000001','Rojer','299','2',NOW());

-- INSERT INTO pricelist (ISBN, sold_by, book_price, piece, added)
-- VALUES('0000000000008','Baboo','999','1',NOW());

-- INSERT INTO pricelist (ISBN, sold_by, book_price, piece, added)
-- VALUES('0000000000005','Rojer','389','1',NOW());

-- INSERT INTO pricelist (ISBN, sold_by, book_price, piece, added)
-- VALUES('0000000000002','Teemo','99','3',NOW());

-- INSERT INTO pricelist (ISBN, sold_by, book_price, piece, added)
-- VALUES('0000000000005','Poesa','389','4',NOW());

-- INSERT INTO pricelist (ISBN, sold_by, book_price, piece, added)
-- VALUES('0000000000003','Chamod','529','3',NOW());

-- INSERT INTO pricelist (ISBN, sold_by, book_price, piece, added)
-- VALUES('0000000000006','Rojer','699','1',NOW());

-- INSERT INTO pricelist (ISBN, sold_by, book_price, piece, added)
-- VALUES('0000000000008','Poesa','899','1',NOW());