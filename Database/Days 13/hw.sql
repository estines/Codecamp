-- show sum of course price
SELECT COUNT(DISTINCT e.student_id) AS Total_student, SUM(c.price) AS Total_price
FROM courses AS c
INNER JOIN enrolls AS e ON e.course_id = c.id 

-- Show course that student taked and price 
SELECT s.id, s.name, COUNT(e.course_id) AS Total_Course, SUM(c.price) AS Total_Price , MAX(c.name) AS Course_name, MAx(c.price) AS Expensive_Course_Price
FROM students AS s
INNER JOIN enrolls AS e ON e.student_id = s.id
INNER JOIN courses AS c ON c.id = e.course_id
GROUP BY s.id;

--------------------------------------------------------------------------
Level              | Dirty reads | Non-repeatable reads | Phantoms reads |
READ UNCOMMITTED   |      Y      |          Y           |       Y        |
READ COMMITTED     |      N      |          Y           |       Y        |
REPEATABLE READ    |      N      |          N           |       Y        | * DEFAULT in SQL
SERIALIZABLE       |      -      |          -           |       -        |
--------------------------------------------------------------------------

set transaction isolation level read uncommitted;
begin;
-- dirty read
select balance from accounts where id = 1;

update accounts set balance = 1000 where id = 1;

select balance from accounts where id = 1;

rollback;

select balance from accounts where id = 1;

-- non-repeatable reads
select balance from accounts where id = 1;

update accounts set balance = 1000 where id = 1;

commit;

select balance from accounts where id = 1;

-- phantom read

select count(*) from accounts;

insert into accounts(id, balance) values ('3','100')

commit;

select count(*) from accounts;

set transaction isolation level read committed;
begin;
    -- dirty read
    select balance
    from accounts
    where id = 1;

    update accounts set balance = 1000 where id = 1;

    select balance
    from accounts
    where id = 1;

    rollback;

    select balance
    from accounts
    where id = 1;

    -- non-repeatable reads
    select balance
    from accounts
    where id = 1;

    update accounts set balance = 1000 where id = 1;

    commit;

    select balance
    from accounts
    where id = 1;

    -- phantom read

    select count(*)
    from accounts;

    insert into accounts
        (id, balance)
    values
        ('3', '100')

    commit;

    select count(*)
    from accounts;

set transaction isolation level repeatable read;
begin;
    -- dirty read
    select balance
    from accounts
    where id = 1;

    update accounts set balance = 1000 where id = 1;

    select balance
    from accounts
    where id = 1;

    rollback;

    select balance
    from accounts
    where id = 1;

    -- non-repeatable reads
    select balance
    from accounts
    where id = 1;

    update accounts set balance = 1000 where id = 1;

    commit;

    select balance
    from accounts
    where id = 1;

    -- phantom read >

    select count(*)
    from accounts;

    insert into accounts
        (id, balance)
    values
        ('3', '100')

    commit;

    select count(*)
    from accounts;

-- Can't run bcoz timeout exceed
set transaction isolation level SERIALIZABLE;
begin;
    -- dirty read
    select balance
    from accounts
    where id = 1;

    update accounts set balance = 1000 where id = 1;

    select balance
    from accounts
    where id = 1;

    rollback;

    select balance
    from accounts
    where id = 1;

    -- non-repeatable reads
    select balance
    from accounts
    where id = 1;

    update accounts set balance = 1000 where id = 1;

    commit;

    select balance
    from accounts
    where id = 1;

    -- phantom read

    select count(*)
    from accounts;

    insert into accounts
        (id, balance)
    values
        ('3', '100')

    commit;

    select count(*)
    from accounts;
