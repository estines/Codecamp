-- Show instructor and price of course that taked
SELECT courses.id, courses.name as course_name, instructors.name as teach_by , courses.price as course_price
FROM courses
INNER JOIN enrolls on enrolls.course_id = courses.id
INNER JOIN instructors on instructors.id = courses.teach_by
GROUP BY courses.id

-- Show instructor and price of course that not taked
SELECT courses.id, courses.name as course_name, instructors.name as teach_by , courses.price as course_price
FROM courses
LEFT JOIN enrolls on enrolls.course_id = courses.id
LEFt JOIN instructors on instructors.id = courses.teach_by
WHERE enrolls.student_id is null
GROUP BY courses.id

-- Show instructor and price of course that not taked and instructor is not null
SELECT courses.id, courses.name as course_name, instructors.name as teach_by , courses.price as course_price
FROM courses
LEFT JOIN enrolls on enrolls.course_id = courses.id
LEFt JOIN instructors on instructors.id = courses.teach_by
WHERE enrolls.student_id is null AND teach_by is not null
GROUP BY courses.id