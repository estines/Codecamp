-- instructors is main
SELECT instructors.id, instructors.name, courses.name
FROM instructors
left join courses on courses.teach_by = instructors.id
WHERE courses.name is null
ORDER BY instructors.id

-- 25 Martin Scorsese NULL
-- 26 Bob Woofward NULL
-- 27 Ron Howard NULL
-- 28 Thomas Keller NULL
-- 29 Alice Waters NULL 
-- 30 Helen Mirren NULL
-- 31 Armin Van Buuren NULL


-- courses in main
SELECT courses.id, courses.name, instructors.name
FROM courses
LEFT JOIN instructors on instructors.id = courses.teach_by
WHERE instructors.name is null
ORDER BY courses.id

-- 25 Database System Concept NULL
-- 26 JavaScript for Beginner NULL
-- 27 OWASP Top 10 NULL