INSERT INTO students (id, name, created_at) VALUES
('1', 'Henry', NOW()),
('2', 'Edword', NOW()),
('3', 'Edrick', NOW()),
('4', 'Allen', NOW()),
('5', 'Joe', NOW()),
('6', 'Nathan', NOW()),
('7', 'Gordon', NOW()),
('8', 'Iris', NOW()),
('9', 'Wally', NOW()),
('10', 'Matt', NOW());

INSERT INTO enrolls (student_id, course_id, enrolled_at) VALUES
('1','2', NOW()),
('2','5', NOW()),
('3','1', NOW()),
('4','15', NOW()),
('5','3', NOW()),
('6','4', NOW()),
('7','3', NOW()),
('8','9', NOW()),
('9','17', NOW()),
('10','2', NOW());

-- Select course that taked
SELECT courses.id, courses.name as course_name, enrolls.student_id
FROM courses
INNER JOIN enrolls on enrolls.course_id = courses.id
GROUP BY courses.id

-- id course_name
-- 1 Cooking
-- 2 Acting
-- 3 Chess
-- 4 Writing
-- 5 Conservation
-- 9 Building a Fashion Brand
-- 15 Film Scoring
-- 17 Writing for Television

-- Select course that not taked
SELECT courses.id, courses.name as course_name
FROM courses
LEFT JOIN enrolls on enrolls.course_id = courses.id
WHERE enrolls.student_id is null
ORDER BY courses.id

-- id course_name
-- 6 Tennis
-- 7 The Art of Performance
-- 8 Writing #2
-- 10 Design and Architecture
-- 11 Singing
-- 12 Jazz
-- 13 Country Music
-- 14 Fashion Design
-- 16 Comedy
-- 18 Filmmaking
-- 19 Dramatic Writing
-- 20 Screenwriting
-- 21 Electronic Music Production
-- 22 Cooking #2
-- 23 Shooting, Ball Handler, and Scoring
-- 24 Photography
-- 25 Database System Concept
-- 26 JavaScript for Beginner
-- 27 OWASP Top 10

