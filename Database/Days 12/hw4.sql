-- 1 )
SELECT d_mng.emp_no, emp.first_name, emp.last_name, dept.dept_name AS Department
FROM dept_manager AS d_mng
INNER JOIN employees AS emp ON emp.emp_no = d_mng.emp_no
LEFT JOIN departments AS dept ON dept.dept_no = d_mng.dept_no
ORDER BY d_mng.emp_no

-- 2 ) ->
-- Employee Salary
select d_emp.emp_no as No , emp.first_name as Firstname, emp.last_name as Lastname , t.title as Title , max(s.salary) as Salary
from dept_emp as d_emp
inner join employees as emp on emp.emp_no = d_emp.emp_no
inner join salaries as s on s.emp_no = d_emp.emp_no
inner join titles as t on t.emp_no = d_emp.emp_no
group by d_emp.emp_no

-- Manager Salary
select d_mng.emp_no, emp.first_name , emp.last_name , t.title as Title , MAX(s.salary) as Salary
from dept_manager as d_mng
inner join employees as emp on emp.emp_no = d_mng.emp_no
inner join salaries as s on s.emp_no = d_mng.emp_no
inner join titles as t on t.emp_no = d_mng.emp_no
group by d_mng.emp_no

-- test
SELECT e.emp_no, e.first_name, e.last_name, t.title, s.salary
FROM employees AS e
    INNER JOIN titles AS t ON t.emp_no = e.emp_no
    INNER JOIN salaries AS s ON s.emp_no = e.emp_no
WHERE s.salary >
(SELECT s.salary
FROM dept_manager)
GROUP BY e.emp_no